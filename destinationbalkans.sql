-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 12, 2016 at 12:46 PM
-- Server version: 5.6.28
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `destinationbalkans`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `db_city_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `db_city_info` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `db_city_description` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `db_city_name`, `db_city_info`, `db_city_description`, `state_id`) VALUES
(1, 'Belgrade', 'Capitol of Serbia<br>1.7million inhabitans<br>Rests on Sava and Danube river<br>\r\nAlso known as Beograd', 'Cool and Old!', 1),
(2, 'Zagreb', 'Capitol of Croatia<br>1.1million inhabitans<br>Rests on Sava river', 'West and Yugoslavia!', 3),
(3, 'Ljubljana', 'capitol of Slovenia', 'some text', 2),
(4, 'Sarajevo', 'Captitol B&H', 'in middle of mountains', 6),
(5, 'Novi Sad', 'Novi Sad is the second largest city in Serbia, the capital of the province of Vojvodina and the administrative seat of the South Bačka District...', 'Novi Sad is a city in Serbia on the banks of the Danube River. Atop a riverside bluff stands Petrovaradin Fortress, much of it dating to the 17th and 18th centuries, with an iconic clock tower and a network of tunnels. Across the river is the old quarter, Stari Grad, site of the Gothic Revival Name of Mary Church and Neo-Renaissance City Hall.', 1),
(6, 'Niš', 'Niš is the city of southern Serbia and the third-largest city in Serbia It is the administrative center of the Nišava District', 'It is one of the oldest cities in the Balkans and Europe, and has from ancient times been considered a gateway between the East and the West.[2] It was founded by the Scordisci in 279 BC, after an invasion of the Balkans. The city was among several taken in the Roman conquest in 75 BC; the Romans built the Via Militaris in the 1st century, with Naissus being one of its key towns; it is also the birthplace of Constantine the Great, the first Christian Emperor and the founder of Constantinople, and Constantius III and Justin I. It is home to one of the oldest churches in Serbia, dating to the 4th century, located in the suburb of Mediana. The Balkans came under the Eastern Roman (Byzantine) Empire. In the 6th century, Slavs started settling the Balkans, while the town was held by the Byzantines until the 9th century, when it came under Bulgar rule. The town switched hands between the two, before being given by the Byzantines to the Serbs in the 12th century. Niš served as Stefan Nemanja\'s capital. It was conquered by the Ottomans in the 15th century, becoming the seat of a sanjak initially in Rumelia Eyalet (1385-1443, 1448-1846), laterly in Niš Eyalet (1846-1864) and finally in Danube Vilayet (1864-1878). It was liberated by the Serbian Army in 1878 during the Serbian–Ottoman War (1876–78).', 1),
(7, 'Skopje', 'Capitol of Macedonia (FYRM)', 'Located in central part of the country', 4),
(8, 'Ohrid', 'a great place', 'a city on a lake', 4),
(9, 'Banja Luka', 'Capitol of Republika Srpska', '2nd most important Serbian city', 6),
(10, 'Kotor', 'ancient fortress town on the coast', 'as beautiful as Dubrovnik, more then 18 centuries old', 5),
(11, 'Split', 'Split is the second-largest city of Croatia and the largest city of the region of Dalmatia. It lies on the eastern shore of the Adriatic Sea, centered on the Roman Palace of the Emperor Diocletian. Spread over a central peninsula and its surroundings, Split\'s greater area includes the neighboring seaside towns as well. An intraregional transport hub and popular tourist destination, the city is a link to numerous Adriatic islands and the Apennine peninsula.', 'Split is one of the oldest cities in the area. While traditionally considered just over 1,700 years old, counting from the construction of Diocletian\'s Palace in 305 CE, the city was in fact founded as the Greek colony of Aspálathos (Aσπάλαθος) in the 4th century BC, about 2,400 years ago. It became a prominent settlement around 650 CE, when it succeeded the ancient capital of the Roman province of Dalmatia, Salona: as after the Sack of Salona by the Avars and Slavs, the fortified Palace of Diocletian was settled by the Roman refugees. Split became a Byzantine city, to later gradually drift into the sphere of the Byzantine vassal, the Republic of Venice, and the Croatian Kingdom, with the Byzantines retaining nominal suzerainty. For much of the High and Late Middle Ages, Split enjoyed autonomy as a free city, caught in the middle of a struggle between Venice and the King of Hungary for control over the Dalmatian cities.', 3),
(12, 'Pula', 'Founded by Romans...', 'Has a coloseum', 3),
(13, 'Mostar', 'Split between religions', 'famous for its bridge', 6),
(14, 'Budva', 'Most popular coastal town on Montenegro seaside', 'Visit Sv.Stefan island', 5),
(15, 'Portotoz', 'Slovenian coastal city', 'nice for chill', 2);

-- --------------------------------------------------------

--
-- Table structure for table `drinking`
--

CREATE TABLE `drinking` (
  `id` int(11) NOT NULL,
  `db_drinking_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_drinking_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_drinking_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_drinking_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `getaways_id` int(11) DEFAULT NULL,
  `national_park_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drinking`
--

INSERT INTO `drinking` (`id`, `db_drinking_name`, `db_drinking_info`, `db_drinking_description`, `db_drinking_location`, `city_id`, `getaways_id`, `national_park_id`) VALUES
(1, 'Bar Central', 'The best cocktails in the city!', 'World Cocktail Champion 2016 will make you a cocktail himself. Need to say more?', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d344.6776997237268!2d20.456127033311343!3d44.82085301921775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x94c66e3c042951d2!2sBar+Central!5e0!3m2!1sen!2srs!4v1478820661011" width', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eating`
--

CREATE TABLE `eating` (
  `id` int(11) NOT NULL,
  `db_eating_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_eating_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_eating_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_eating_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `getaways_id` int(11) DEFAULT NULL,
  `national_park_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eating`
--

INSERT INTO `eating` (`id`, `db_eating_name`, `db_eating_info`, `db_eating_description`, `db_eating_location`, `city_id`, `getaways_id`, `national_park_id`) VALUES
(1, 'Loki', 'Locals late night fast food', 'Traditional Serbian burgers called pljeskavica (plyeskaveetza)...', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d344.6689394166726!2d20.458310519499125!3d44.822318361312604!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd68da41c205e0c4f!2sLoki!5e0!3m2!1sen!2srs!4v1478820462008" width="600"', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `getaways`
--

CREATE TABLE `getaways` (
  `id` int(11) NOT NULL,
  `db_getaways_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `db_getaways_info` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `db_getaways_description` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `getaways`
--

INSERT INTO `getaways` (`id`, `db_getaways_name`, `db_getaways_info`, `db_getaways_description`, `state_id`) VALUES
(1, 'Lake Bled', 'Lake Bled is a lake in the Julian Alps of the Upper Carniolan region of northwestern Slovenia, where it adjoins the town of Bled. The area is a tourist destination. The lake is 35 km (22 mi) from Ljubljana International Airport and 55 km (34 mi) from the capital city, Ljubljana.', 'The lake surrounds Bled Island (Blejski otok). The island has several buildings, the main one being the pilgrimage church dedicated to the Assumption of Mary (Cerkev Marijinega vnebovzetja), built in its current form near the end of the 17th century, and decorated with remains of Gothic frescos from around 1470 in the presbyterium and rich Baroque equipment.[1] The church has a 52 m (171 ft) tower and there is a Baroque stairway from 1655 with 99 stone steps leading up to the building. The church is frequently visited and weddings are held there regularly. Traditionally it is considered good luck for the groom to carry his bride up the steps on the day of their wedding before ringing the bell and making a wish inside the church.', 2),
(2, 'Kopaonik', 'Kopaonik, is one of the larger mountain ranges of Serbia. It is located in the central part of Serbia. Its highest point, Pančić\'s Peak, is 2,017 m (6,617 ft) above sea level.', 'Kopaonik is the major ski resort of Serbia, and after Bulgaria\'s Bansko, largest in Southeast Europe. It boasts total of 25 ski lifts with capacity of 32,000 skiers per hour.[4][5] A national park spread over 118.1 km2 (45.6 sq mi) of the central part of the Kopaonik plateau was established in 1981', 1),
(3, 'Matka', 'Matka (Macedonian: Матка, meaning "womb") is a canyon located west of central Skopje, Macedonia. Covering roughly 5,000 hectares,Matka is one of the most popular outdoor destinations in Macedonia and is home to several medieval monasteries. The Matka Lake within the Matka Canyon is the oldest artificial lake in the country.', 'There are ten caves at Matka Canyon, with the shortest in length being 20 metres (65.6 feet) and the longest being 176 metres (577.4 feet). The canyon also features two vertical pits, both roughly extending 35 metres (114.8 feet) in depth. \r\nMatka Canyon is home to a wide variety of plants and animals, some of which is unique to the area. Roughly 20% of plant life is found only at Matka. The canyon is also home to 77 indigenous species of butterfly.\r\nThe canyon\'s caves are home to large populations of bats', 4),
(4, 'Jahorina', 'Jahorina is a mountain in Bosnia and Herzegovina, located near Pale in the Dinaric Alps. It borders Mount Trebević, another Olympic mountain. Jahorina\'s highest peak, Ogorjelica, has a summit elevation of 1,916 metres (6,286 ft), making it the second-highest of Sarajevo\'s mountains', 'Jahorina Olympic Ski Resort is the biggest and most popular ski resort in Bosnia and Herzegovina and offers a variety of outdoor sports and activities. It is primarily a destination for alpine skiing, snowboarding, hiking, and sledding, with over 40 km (25 mi) of ski slopes and modern facilities. The average snow depth on ski runs during February is 106 cm (42 in) (ten-year average).\r\n\r\nThe Jahorina ski lift system was upgraded in 2012 and 2013 with new Leitner chairlifts. Together with a gondola lift (under construction), Jahorina has one of the most modern lift systems in the region. An 8-passenger gondola will connect the town of Pale and the ski resort, and open up 15 km (9 mi) of new ski runs.', 6),
(5, 'Cetinje', 'Cetinje is a city and Old Royal Capital (Montenegrin: Prijestonica / Приjестоница) of Montenegro. It is also the historic and the secondary capital of Montenegro, where the official residence of the President of Montenegro is located. It had a population of 13,991 as of 2011.', 'Cetinje is the centre of Cetinje Municipality (population 16,757 in 2011). The city rests on a small karst plain surrounded by limestone mountains, including Mt. Lovćen, the Black Mountain from which Montenegro derives its name.\r\n\r\nCetinje is a town of great historical significance, being founded in the 15th century and because it became the center of Montenegrin life and both a cradle of Montenegrin culture and an Orthodox religious center. Its status as the honorary capital of Montenegro is due to its heritage as a long-serving former capital of Montenegro.', 5),
(6, 'Medvedgrad', 'Medvedgrad (pronounced [mědʋedɡraːd]; Croatian for bear-town or city of bears; Hungarian: Medvevár) is a medieval fortified town located on the south slopes of Medvednica mountain, approximately halfway from the Croatian capital Zagreb to the mountain top Sljeme', ' For defensive purposes it was built on a hill, Mali Plazur, that is a spur of the main ridge of the mountain that overlooks the city. On a clear day the castle can be seen from far away, especially the high main tower. Below the main tower of the castle is Oltar Domovine (Altar of the homeland) which is dedicated to Croatian soldiers killed in the Croatian War of Independence', 3),
(7, 'Tara rafting', 'two days in middle of nowhere', 'City stress type of a getaway', 1),
(8, 'Samobor', 'Couples retreat in vicinity of Zagreb', 'romantic getaway with great food, wine and walks trough the forrest', 3);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `db_image_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_image_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_filename_property` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `getaways_id` int(11) DEFAULT NULL,
  `national_park_id` int(11) DEFAULT NULL,
  `sights_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `db_image_file`, `db_image_name`, `db_filename_property`, `updated_at`, `city_id`, `state_id`, `getaways_id`, `national_park_id`, `sights_id`) VALUES
(2, '845a8502088838a49318dc7e41aabf48.jpeg', 'kopaonik', 'kopaonik, serbia', '2017-01-01 00:00:00', NULL, 1, 2, NULL, NULL),
(3, 'e3b3106280c018a7c98013b12b54a0f7.jpeg', 'taramountain', 'tara in summer', '2011-01-01 00:00:00', NULL, 1, NULL, 3, NULL),
(4, '1f8e994f25d07e1ad175d31545e74e03.jpeg', 'tara canyon, montenegro', 'canyon, rafting, nature', '2011-01-01 00:00:00', NULL, 5, NULL, 9, NULL),
(5, 'f006f83ee9c9888bdcf9a3bc719a034c.jpeg', 'durmitor mountain, montenegro', 'mountain, hiking, nature, lakes', '2016-01-01 00:00:00', NULL, 5, NULL, 8, NULL),
(6, '81174035591b96d385f106c67a2e6fbe.jpeg', 'triglav national park, slovenia', 'mountain, hiking, nature, lakes', '2016-01-01 00:00:00', NULL, 2, NULL, 7, NULL),
(7, 'adb59efd030667d24dd027bd48b6613c.jpeg', 'mavrovo lake, macedonia', 'lake, chill, nature', '2016-01-01 00:00:00', NULL, 4, NULL, 6, NULL),
(8, 'cbe4de03468ecf58b2ef9eaa873eeac1.jpeg', 'una river and waterfall, bosnia and herzegovina', 'lake, swimming, waterfall, nature', '2016-01-01 00:00:00', NULL, 6, NULL, 5, NULL),
(9, '55c9660d6a432a98d310f55c9fe51c1e.jpeg', 'obedska bara, serbia', 'swamp, bird watching, fishing, hiking', '2016-01-01 00:00:00', NULL, 1, NULL, 2, NULL),
(10, '7dacb94fab9fe4ba6e8b801c575b5c71.jpeg', 'kotor town, montenegro', 'sightseing, visiting, weekend away', '2016-01-01 00:00:00', 10, 5, NULL, NULL, NULL),
(11, '7b13505a3bda36bd20c60f8d9e4cc8cc.png', 'banja luka, bosnia and herzegovina', 'sightseing, visiting, weekend away, republika srpska', '2016-01-01 00:00:00', 9, 6, NULL, NULL, NULL),
(12, 'd51b5ea11dd16340f761a30771f000d5.jpeg', 'ohrid town', 'sightseing, visiting, weekend away, swimming, lake', '2016-01-01 00:00:00', 8, 4, NULL, NULL, NULL),
(13, '17781ac2c37ffe5f6bec4e61243afd69.jpeg', 'skopje, macedonia', 'sightseing, visiting, weekend away, capitol city', '2016-01-01 00:00:00', 7, 4, NULL, NULL, NULL),
(14, '6091334a250110471022106779c7f475.jpeg', 'nis, serbia', 'medeival, sightseing, stop over', '2016-01-01 00:00:00', 6, 1, NULL, NULL, NULL),
(15, 'fe862c14d546861d69442d27f7c525db.jpeg', 'novi sad, serbia', 'sightseing, visiting, weekend away, vojvodina, culture visit', '2016-01-01 00:00:00', 5, 1, NULL, NULL, NULL),
(16, '2cfd9bed734b5dcebf8dd5edca0c5832.jpeg', 'sarajevo, bosnia and herzegovina', 'sightseing, visiting, weekend away, culture clash, islamic, war, culture visit', '2016-01-01 00:00:00', 4, 6, NULL, NULL, NULL),
(17, 'd6001170c5d3cb4c3ccf37db902ecf0a.jpeg', 'ljubljana, slovenia', 'sightseing, visiting, weekend away, culture visit, architecture, peacefull', '2016-01-01 00:00:00', 3, 2, NULL, NULL, NULL),
(18, 'd4250f982279ce18b9d7966d1b3d2064.jpeg', 'zagreb, croatia', 'sightseing, visiting, weekend away, culture visit, capitol city', '2016-01-01 00:00:00', 2, 3, NULL, NULL, NULL),
(19, 'cf91d3758d779ea94b20a9c4d718841f.jpeg', 'belgrade, serbia', 'sightseing, visiting, weekend away, culture visit, capitol city, nightlife', '2016-11-16 00:00:00', 1, 1, NULL, NULL, NULL),
(20, '4cde4f3d8b0c306ec1b9f2a881d858a2.jpeg', 'split, croatia', 'coast, swimming, romantic, seaside', '2016-01-01 00:00:00', 11, 3, NULL, NULL, NULL),
(21, 'e708b81e16dcc379e16f66bcd95a56f9.jpeg', 'plitvice national park', 'national park, croatia, day trip', '2011-01-01 00:00:00', NULL, 3, NULL, 4, NULL),
(23, '38a57f466a4957f657b73eb6c5268002.jpeg', 'Samobor, canal view', 'romantic, weekend getaway, lodging, food, eating', '2011-01-01 00:00:00', NULL, 3, 8, NULL, NULL),
(24, 'c81888b13c780a9d87cb86a3dac46947.jpeg', 'Jahorina, Bosnia and Herzegovina', 'skiing, mountain, winter, resort', '2011-01-01 00:00:00', NULL, 6, 4, NULL, NULL),
(25, '8140a337d2be0a9a9ce85e94308e6602.jpeg', 'Cetinje, Montenegro', 'mountain, hiking, nature, sleep-over', '2011-01-01 00:00:00', NULL, 5, 5, NULL, NULL),
(26, '333d8e5a89ec8059f0919f6649b064af.jpeg', 'Matka canyon, Macedonia', 'canyon, rafting, nature, a must do', '2011-01-01 00:00:00', NULL, 4, 3, NULL, NULL),
(27, '9343eea8f8d431278e663c6edef133d3.jpeg', 'Medvedgrad, Croatia', 'day trip, hiking, skiing, near city getaway', '2011-01-01 00:00:00', NULL, 3, 6, NULL, NULL),
(29, '412e3a65dd28a4de98f4b8c24c55a8ce.jpeg', 'Bled lake, Slovenia', 'romantic, weekend getaway, lodging, food, eating, couples retreat', '2011-01-01 00:00:00', NULL, 2, 1, NULL, NULL),
(30, 'd02db0e558ce340e4c44ed4ce547efb1.jpeg', 'Tara rafting, Montenegro', 'rafting, sport, activity, nature, camping', '2011-01-01 00:00:00', NULL, 5, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `national_park`
--

CREATE TABLE `national_park` (
  `id` int(11) NOT NULL,
  `db_park_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `db_park_info` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `db_park_description` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `national_park`
--

INSERT INTO `national_park` (`id`, `db_park_name`, `db_park_info`, `db_park_description`, `state_id`) VALUES
(1, 'Uvac', '"Uvac" Special Nature Reserve is protected natural asset of great importance, situated in south-western Serbia in the region of Stari Vlah-Raška high plateau, wedged between Mt.Zlatar massif in the Southwest and Mt. Javor in the Northeast. ', '"Uvac" Special Nature Reserve is protected natural asset of great importance, situated in south-western Serbia in the region of Stari Vlah-Raška high plateau, wedged between Mt.Zlatar massif in the Southwest and Mt. Javor in the Northeast. \n\nMinimum altitude in the reserve is 760m above sea level, and maximum 1322 m.\n\nMajor morphological feature of the reserve is the Uvac River canyon valley including the valleys of its tributaries. The special value of canyon parts of the river valley are curving meanders. Relative height of meander heads is 100m at places.\n\nThe surrounding is characterised by karst surface with numerous karst formations: karst plains, karst depressions, karst sinkholes, rock shelters, caves and potholes. Caves are numerous and vary in size, ranging from rock shelters to  Usak Cave System, the largest known cave system in Serbia (6185 м). Caves are very rich in speleothem deposited by the action of dripping water to form stalactites, stalagmites, columns, draperies, transparent needles etc.\nGriffon Vulture (Gyps fulvus) "Uvac" Special Nature Reserve is distinguished by the presence of 104 bird species. Most important of all isgriffon vulture, one of the two vulture species that are still nesting in Serbia.\n\nGriffon vultures are, at the same time, the greatest attraction in the reserve. Griffon vulture is a vulture species, of an impressive size with a wingspan of 3m at times, which makes him a powerful flier. Its flight has been studied by scientists – aeronautic engineers that used the knowledge on construction of aircrafts. The role it plays in the food chain is unique and makes him irreplaceable. Its is adopted to a diet of dead animals and therefore stops the spread of diseases and contributes to a kind of "natural recycling".', 1),
(2, 'Obedska Bara', 'Obedska bara is a large swamp-forest area and natural reserve stretching along the Sava River in Southern Syrmia (Serbia), some 40 km west of Belgrade...', 'Obedska bara is a large swamp-forest area and natural reserve stretching along the Sava River in Southern Syrmia (Serbia), some 40 km west of Belgrade.\r\n\r\nThe pond is an authentic complex of stagnant tributaries, marshes, pits, marsh vegetation, damp meadows and forests. It is home to over 30 different water, swamp, forest and meadow biocoenoses. \r\nThe fauna includes 220 species of birds, 50 species of mammals, 13 of amphibians, 11 of reptiles and 16 of fish, while the flora includes 500 species plants, 180 species of mushrooms and 50 species of moss. It is one of the richest and best preserved wildlife habitats in the Pannonian plain.\r\n\r\nObedska pond is one of the world\'s oldest nature areas, first administrative protective measures having been introduced 1874, when the Habsburg Empire protected it as hunting ground of the royal family. \r\nPresently, it has first-category legal protection status, denoting a natural asset of exceptional value. Its status has been verified by the Ramsar Convention on swamps since 1977, and included in the List of areas of special significance for birds of Europe of Important Bird Area project, and UNESCO\'s list of world\'s most important wetland areas.', 1),
(3, 'Tara (mountain)', 'Mountain Tara belongs to internal Dinarides and it is part of Serbian Vlach mountains. It is located In the far West of Serbia in encompassing area bounded by the Drina River between Visegrad and Bajina Basta. In the narrowest part of the mountain massif Mt Zvijezda was placed and it is naturally separated from Mt Tara by Canyon of Derventa River. Looking by altitudes, Tara is medium-high mountain, with an mean altitude of 1,000-1,200 meters above sea level. The highest peak is Kozji (Goat) rid with high of 1,591 meters.', 'Mountain Tara belongs to internal Dinarides and it is part of Serbian Vlach mountains. It is located In the far West of Serbia in encompassing area bounded by the Drina River between Visegrad and Bajina Basta. \r\nIn the narrowest part of the mountain massif Mt Zvijezda was placed and it is naturally separated from Mt Tara by Canyon of Derventa River. Looking by altitudes, Tara is medium-high mountain, with an mean altitude of 1,000-1,200 meters above sea level. The highest peak is Kozji (Goat) rid with high of 1,591 meters.\r\nTara is a typical forest area, and for its preservation and diversity of forest ecosystems (many of which are relict) one of the richest and most valuable forest areas in Europe. \r\nIn forest sense Tara are covered with mixed forests of European Spruce, Silver Fir and European Beech (over 85 % of forest area) and specificity compared to other mountains of the Balkan Peninsula is the large number of relict and endemic forest species and plant communities.\r\n\r\nAt Tara was identified more than 40 broadleaf, deciduous - coniferous and coniferous phytocenoses, then 1,156 species of vascular flora which makes 1/3 of the total flora of Serbia. From represented 76 plant species are endemic. Special value and importance of the Serbian Spruce (Picea omorika), endemic and relict species, which is inhabits canyons and ravines of the middle course of the Drina River managed to survive the last ice age.\r\nAccording to the results of previous research, an area inhabited by Tara over 5O species of mammals, 140 species of birds, 23 species of amphibians and reptiles and 19 species of fish. \r\nThis area is inhabited by the largest population of brown bears in Serbia. The best-known species, for which it may be said to be symbol of fauna Mt Tara, is endemorelict Pancic\'s grasshopper (Pyrgomorphella serbica).', 1),
(4, 'Plitvice Lakes', 'The national park was founded in 1949 and is situated in the mountainous karst area of central Croatia, at the border to Bosnia and Herzegovina. The important north-south road connection, which passes through the national park area, connects the Croatian inland with the Adriatic coastal region.\r\n\r\nThe protected area extends over 296.85 square kilometres (73,350 acres). About 90% of this area is part of Lika-Senj County, while the remaining 10% is part of Karlovac County.', 'The national park is world-famous for its lakes arranged in cascades. Currently, 16 lakes can be seen from the surface.[6] These lakes are a result of the confluence of several small rivers and subterranean karst rivers. The lakes are all interconnected and follow the water flow. They are separated by natural dams of travertine, which is deposited by the action of moss, algae, and bacteria. The particularly sensitive travertine barriers are the result of an interplay between water, air and plants. The encrusted plants and bacteria accumulate on top of each other, forming travertine barriers which grow at the rate of about 1 cm (0.4 in) per year.\r\n\r\nThe sixteen lakes are separated into an upper and lower cluster formed by runoff from the mountains, descending from an altitude of 636 to 503 m (2,087 to 1,650 ft) over a distance of some eight km, aligned in a south-north direction. The lakes collectively cover an area of about two square kilometres (0.77 square miles), with the water exiting from the lowest lake forming the Korana River.', 3),
(5, 'Una', 'Una National Park (Bosnian: Nacionalni park Una) was established in 2008 around the Upper Una River and the Unac River. It is Bosnia and Herzegovina’s most recently established national park. The main purpose of the park is to protect the unspoiled Una and Unac rivers which run through it.', 'Protection zone of the National Park stretches on the western side from the source of the Krka creek and its course to the confluence with the Una on the state border of Bosnia and Herzegovina with Croatia from where park border follows the Una and state border to the town of Martin Brod and confluence with the Unac. On the eastern side border of the park goes from the entrance of the Unac River into its canyon, few kilometers downstream from town of Drvar, and follows the Unac and its canyon all the way to the confluence with the Una in town of Martin Brod. From there park border follows the Una on the right and state border between Bosnia and Herzegovina and Croatia on the left, until it reach a small town of Ripač, few kilometers upstream from town of Bihać.', 6),
(6, 'Mavrovo', 'Mavrovo National Park (Macedonian: Национален парк Маврово) is the largest of the three national parks of the Republic of Macedonia. It was founded in 1949 and is located in the west of the country between Lake Mavrovo and the Albanian border.', '\r\nThe national park, the lake, and the region are named after the village of Mavrovo.', 4),
(7, 'Triglav', 'Triglav National Park (TNP) (Slovene: Triglavski narodni park) is the only national park in Slovenia. It was established in its modern form in 1981 and is located in the northwestern part of the country, respectively the southeastern part of the Alpine massif', 'Mount Triglav, the highest peak of Julian Alps, stands almost in the middle of the national park. From it the valleys spread out radially, supplying water to two large river systems with their sources in the Julian Alps: the Soča and the Sava, flowing to the Adriatic and Black Sea, respectively.', 2),
(8, 'Durmitor', 'The Durmitor National Park, created in 1952, includes the massif of Durmitor, the canyons of Tara, Sušica and Draga rivers and the higher part of the canyon plateau Komarnica, covering the area of 390 km².', 'The Durmitor National Park, created in 1952, includes the massif of Durmitor, the canyons of Tara, Sušica and Draga rivers and the higher part of the canyon plateau Komarnica, covering the area of 390 km².\r\n\r\nIt was inscribed on the list of UNESCO World Heritage Sites in 1980.\r\n\r\n80 kilometers long and 1,300 meters deep, the Tara River Canyon in the Durmitor National Park is the second largest in the world, after the Grand Canyon.\r\nDurmitor mountain is the centre of Montenegrin mountain tourism. The tourist facilities are concentrated around the town of Žabljak.\r\n\r\nDuring winter, the main activities on Durmitor are skiing and, increasingly, snowboarding. In the summer, the activities shift to hiking, mountaineering and recreational tourism. One of the most prominent attraction of Durmitor mountain are 18 glacial lakes, best known being Crno Lake.', 5),
(9, 'Tara (canyon)', '2nd deepest canyon in the world', 'the only rival to the Grand Canyon', 5);

-- --------------------------------------------------------

--
-- Table structure for table `sights`
--

CREATE TABLE `sights` (
  `id` int(11) NOT NULL,
  `db_sights_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `db_sights_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_sights_description` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sights`
--

INSERT INTO `sights` (`id`, `db_sights_name`, `db_sights_info`, `db_sights_description`, `city_id`) VALUES
(1, 'Kalemegdan', 'Central part of Belgrade upper town', 'the main and most important landmark of Belgrade. \r\nHint! "watch sunsets"', 1),
(2, 'St. Marks Church', 'Symbol of the city', 'Free entrance', 2),
(3, 'Ohrid lake', 'great for swimming and the city lays on the shores of it', 'one of the largest lakes in Europe', 8),
(4, 'Diocletian\'s Palace', 'The main part of the old town', 'come in the night for some music and wine', 11),
(5, 'Three bridges of Ljubljana', 'One of the landmarks of the old town', 'great Architectural idea', 3),
(6, 'Bascarsija (Baščaršija)', 'The main meeting point', 'A symbol every one knows Sarajevo by', 4),
(7, 'Petrovaradin fortress', 'The place of the exit festival', 'Great for walks and overview of the city', 5),
(8, 'Skull tower', 'the tower has become a symbol of Serbian independence', 'The Skull Tower is a tower composed largely of human skulls located in the city of Niš, Serbia. During the 1809 Battle of Čegar, fought during the First Serbian Uprising (part of the Serbian Revolution, 1804–1817), Serbian revolutionaries under Stevan Sinđelić were attacked by Turkish forces on Čegar Hill, near Niš. Rather than be captured by the Turks and impaled, Sinđelić fired his pistol into a powder magazine, killing himself and all Serbian rebels and Turkish soldiers in the vicinity. Afterward, Hurshid Pasha, the Turkish vizier of Niš, ordered that a tower be made from the skulls of the killed Serbian revolutionaries.', 6),
(9, 'The monument of Alexander the Great', 'the main square of Skopje', 'build in the modern era Skopje', 7),
(10, 'Kastel of Banja Luka', 'historical sight of the city', 'lies on Vrbas river, free entrance', 9),
(11, 'The Cathedral of Saint Tryphon', 'The Kotor Cathedral is a Roman Catholic cathedral that was consecrated on June 19, 1166', 'The Kotor Cathedral is a Roman Catholic cathedral that was consecrated on June 19, 1166.[1] Compared to other buildings, the Kotor Cathedral is one of the largest and most ornate buildibgs in Kotor. The cathedral was seriously damaged and rebuilt after the earthquake of 1667, but there were not enough funds for its complete reconstruction[citation needed]. Another massive earthquake in April 1979, which completely devastated the Montenegro coast, also greatly damaged the cathedral. Luckily, it has been salvaged and the careful restoration of parts of its interior has not been completed until a few years ago[citation needed]. Today, this formidable piece of Romanesque architecture, one of the oldest and perhaps the most beautiful monument along the Adriatic Sea, is showing its splendor again.\r\n\r\nIt contains a rich collection of artifacts. Older than many famous churches and cathedrals in Europe, the Cathedral of St. Tryphon has a treasury of immense value. In its interior there are frescos from the 14th century, a stone ornament above the main altar in which the life of St. Tryphon is depicted, as well as a relief of saints in gold and silver. The most representative works of Kotor\'s masters and craftsmen are kept in this Cathedral, making its collection quite unique.\r\n\r\nThe collection of art objects includes a silver hand and a cross, decorated with ornaments and figures in relief[citation needed]. It is only a part of the valuable objects of the Treasury of this unique sacral building which was the City Hall in the past.\r\n\r\nToday, it is the best known tourist attraction in Kotor and a symbol of the city: the Saint is depicted in the city’s coat of arms, along with a lion and the Mount of San Giovanni (St. John).', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sleeping`
--

CREATE TABLE `sleeping` (
  `id` int(11) NOT NULL,
  `db_sleeping_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_sleeping_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `db_sleeping_description` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `db_sleeping_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `getaways_id` int(11) DEFAULT NULL,
  `national_park_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sleeping`
--

INSERT INTO `sleeping` (`id`, `db_sleeping_name`, `db_sleeping_info`, `db_sleeping_description`, `db_sleeping_location`, `city_id`, `getaways_id`, `national_park_id`) VALUES
(1, 'Star Hostel Belgrade', 'With 11 years of experience behind the team', 'Hostel, dorms from 10-15euros a night, private from 35euros a night', 'Hostel, dorms from 10-15euros a night, private from 35euros a night', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `db_state_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `db_state_info` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `db_state_description` varchar(2048) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `db_state_name`, `db_state_info`, `db_state_description`) VALUES
(1, 'Serbia', 'Crossroad between WEST AND EAST...', 'Following the Slavic migrations to the Balkans from the 6th century onwards, Serbs established several states in the early Middle Ages. The Serbian Kingdom obtained recognition by Rome and the Byzantine Empire in 1217; it reached its peak in 1346 as a relatively short-lived Serbian Empire. By the mid-16th century, the entire modern-day Serbia was annexed by the Ottomans, at times interrupted by the Habsburg Empire, which started expanding towards Central Serbia since the end of the 17th century, while maintaining foothold in modern-day Vojvodina. In the early 19th century, the Serbian Revolution established the nation-state as the region\'s first constitutional monarchy, which subsequently expanded its territory.'),
(2, 'Slovenia', 'Slovenia, a country in Central Europe, is known for its mountains, ski resorts and lakes...', 'Slovenia, a country in Central Europe, is known for its mountains, ski resorts and lakes. On Lake Bled, a glacial lake fed by hot springs, the town of Bled contains a church-topped islet and a cliffside medieval castle. In Ljubljana, Slovenia’s capital, baroque facades mix with the 20th-century architecture of native Jože Plečnik, whose iconic Tromostovje (Triple Bridge) spans the tightly curving Ljubljanica River.'),
(3, 'Croatia', 'Croatia is an Eastern European country with a long coastline on the Adriatic Sea. Encompassing more than a thousand islands, it\'s also crossed by the Dinaric Alps...', 'Croatia is an Eastern European country with a long coastline on the Adriatic Sea. Encompassing more than a thousand islands, it\'s also crossed by the Dinaric Alps. Its inland capital, Zagreb, is distinguished by its medieval Gornji Grad (Upper Town) and diverse museums. The major coastal city Dubrovnik has massive 16th-century walls encircling an Old Town with Gothic and Renaissance buildings.'),
(4, 'Macedonia', 'Macedonia is a landlocked Balkan nation of mountains, lakes and ancient towns with Ottoman and European architecture...', 'Macedonia is a landlocked Balkan nation of mountains, lakes and ancient towns with Ottoman and European architecture. The capital, Skopje, is known for its sprawling Old Bazaar quarter and historic buildings turned museums, including the National Gallery of Macedonia, housed in a 15th-century Turkish bath complex. The southern city Ohrid, on a lake of the same name, has a medieval townscape and hilltop castle.'),
(5, 'Montenegro', 'Montenegro is a Balkan country with rugged mountains, medieval villages and a narrow strip of beaches along its Adriatic coastline...', 'Montenegro is a Balkan country with rugged mountains, medieval villages and a narrow strip of beaches along its Adriatic coastline. The Bay of Kotor, resembling a fjord, is dotted with coastal churches and fortified towns such as Kotor and Herceg Novi. Durmitor National Park, home to bears and wolves, encompasses limestone peaks, glacial lakes and 1,300m-deep Tara River Canyon.'),
(6, 'Bosnia and Herzegovina', 'Bosnia and Herzegovina is a country on the Balkan Peninsula in southeastern Europe. Its countryside is home to medieval villages, rivers and lakes, plus the craggy Dinaric Alps...', 'Bosnia and Herzegovina is a country on the Balkan Peninsula in southeastern Europe. Its countryside is home to medieval villages, rivers and lakes, plus the craggy Dinaric Alps. National capital Sarajevo has a well preserved old quarter, Baščaršija, with landmarks like 16th-century Gazi Husrev-bey Mosque. Ottoman-era Latin Bridge is the site of the assassination of Archduke Franz Ferdinand, which ignited World War I.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_2D5B02344B4234E4` (`db_city_name`),
  ADD KEY `IDX_2D5B02345D83CC1` (`state_id`);

--
-- Indexes for table `drinking`
--
ALTER TABLE `drinking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C00415EB8BAC62AF` (`city_id`),
  ADD KEY `IDX_C00415EBFDFD2312` (`national_park_id`),
  ADD KEY `IDX_C00415EB6A876C2B` (`getaways_id`);

--
-- Indexes for table `eating`
--
ALTER TABLE `eating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C65A14018BAC62AF` (`city_id`),
  ADD KEY `IDX_C65A1401FDFD2312` (`national_park_id`),
  ADD KEY `IDX_C65A14016A876C2B` (`getaways_id`);

--
-- Indexes for table `getaways`
--
ALTER TABLE `getaways`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DA6FDFA85D83CC1` (`state_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E01FBE6A8BAC62AF` (`city_id`),
  ADD KEY `IDX_E01FBE6A5D83CC1` (`state_id`),
  ADD KEY `IDX_E01FBE6A6A876C2B` (`getaways_id`),
  ADD KEY `IDX_E01FBE6AFDFD2312` (`national_park_id`),
  ADD KEY `IDX_E01FBE6AA6EDFA2D` (`sights_id`);

--
-- Indexes for table `national_park`
--
ALTER TABLE `national_park`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_60D330792024C74F` (`db_park_name`),
  ADD KEY `IDX_60D330795D83CC1` (`state_id`);

--
-- Indexes for table `sights`
--
ALTER TABLE `sights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CA3C02A68BAC62AF` (`city_id`);

--
-- Indexes for table `sleeping`
--
ALTER TABLE `sleeping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_691EBBE8BAC62AF` (`city_id`),
  ADD KEY `IDX_691EBBEFDFD2312` (`national_park_id`),
  ADD KEY `IDX_691EBBE6A876C2B` (`getaways_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `drinking`
--
ALTER TABLE `drinking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `eating`
--
ALTER TABLE `eating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `getaways`
--
ALTER TABLE `getaways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `national_park`
--
ALTER TABLE `national_park`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sights`
--
ALTER TABLE `sights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sleeping`
--
ALTER TABLE `sleeping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `FK_2D5B02345D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`);

--
-- Constraints for table `drinking`
--
ALTER TABLE `drinking`
  ADD CONSTRAINT `FK_C00415EB6A876C2B` FOREIGN KEY (`getaways_id`) REFERENCES `getaways` (`id`),
  ADD CONSTRAINT `FK_C00415EB8BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_C00415EBFDFD2312` FOREIGN KEY (`national_park_id`) REFERENCES `national_park` (`id`);

--
-- Constraints for table `eating`
--
ALTER TABLE `eating`
  ADD CONSTRAINT `FK_C65A14016A876C2B` FOREIGN KEY (`getaways_id`) REFERENCES `getaways` (`id`),
  ADD CONSTRAINT `FK_C65A14018BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_C65A1401FDFD2312` FOREIGN KEY (`national_park_id`) REFERENCES `national_park` (`id`);

--
-- Constraints for table `getaways`
--
ALTER TABLE `getaways`
  ADD CONSTRAINT `FK_DA6FDFA85D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`);

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK_E01FBE6A5D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  ADD CONSTRAINT `FK_E01FBE6A6A876C2B` FOREIGN KEY (`getaways_id`) REFERENCES `getaways` (`id`),
  ADD CONSTRAINT `FK_E01FBE6A8BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_E01FBE6AA6EDFA2D` FOREIGN KEY (`sights_id`) REFERENCES `sights` (`id`),
  ADD CONSTRAINT `FK_E01FBE6AFDFD2312` FOREIGN KEY (`national_park_id`) REFERENCES `national_park` (`id`);

--
-- Constraints for table `national_park`
--
ALTER TABLE `national_park`
  ADD CONSTRAINT `FK_60D330795D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`);

--
-- Constraints for table `sights`
--
ALTER TABLE `sights`
  ADD CONSTRAINT `FK_CA3C02A68BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`);

--
-- Constraints for table `sleeping`
--
ALTER TABLE `sleeping`
  ADD CONSTRAINT `FK_691EBBE6A876C2B` FOREIGN KEY (`getaways_id`) REFERENCES `getaways` (`id`),
  ADD CONSTRAINT `FK_691EBBE8BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `FK_691EBBEFDFD2312` FOREIGN KEY (`national_park_id`) REFERENCES `national_park` (`id`);
