<?php

namespace AppBundle\Repository;

/**
 * ImagesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ImagesRepository extends \Doctrine\ORM\EntityRepository
{
    public function findOneByDbImageFileJoinedToState($dbStateName)
    {
      $query = $this->getEntityManager()
     ->createQuery(
             'a.dbImageFile,
             b.dbCityName,
             b.dbCityInfo,
             c.dbStateName,
             c.dbStateInfo,
             c.dbStateDescription
          FROM AppBundle:Images a

          JOIN AppBundle:City b with b.id = a.city

          JOIN AppBundle:State c with c.id = a.state

          WHERE c.dbStateName = $dbStateName')
        ;

        try
        {
          return $query->getMaxResults();
        }
        catch (\Doctrine\ORM\NoResultException $e)
        {
       return null;
        }
    }
}
