<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * NationalParksRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NationalParksRepository extends EntityRepository
{
}
