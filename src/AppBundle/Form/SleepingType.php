<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SleepingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dbSleepingName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
                ->add('dbSleepingInfo', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
                ->add('dbSleepingWeb', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
                ->add('dbSleepingLocation', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
                ->add('dbSleepingStatus', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
                ->add('updatedAt', DateTimeType::class)
                ->add('city', EntityType::class,
                array('class' => 'AppBundle:City',
                    'choice_label' => 'dbCityName' ,
                    'expanded' => false), array('attr' => array('class' => 'selectpicker', 'data-style' => 'btn-info')) )
                ->add('Submit', SubmitType::class, array('attr' => array('class' => 'btn btn-success btn-group-justified', 'style' => 'margin-bottom: 15px')) )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Sleeping'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_sleeping';
    }


}
