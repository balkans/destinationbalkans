<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dbStateName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
            ->add('dbStateInfo', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
            ->add('dbStateDescription', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
            ->add('Submit', SubmitType::class, array('attr' => array('class' => 'btn btn-success btn-group-justified', 'style' => 'margin-bottom: 15px')) )
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\State'
        ));
    }
}
