<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ImagesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dbImageFile', FileType::class, array('attr' => array('class' => 'btn btn-info btn-group-justified')) )
                ->add('dbImageName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
                ->add('dbFilenameProperty', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
                ->add('updatedAt', DateTimeType::class)
                ->add('state', EntityType::class,
                      array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:State',
                            'choice_label' => 'dbStateName' ,
                            'expanded' => false) )
                ->add('city', EntityType::class,
                      array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:City',
                            'choice_label' => 'dbCityName' ,
                            'expanded' => false) )
                ->add('getaways', EntityType::class,
                      array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:Getaways',
                            'choice_label' => 'dbGetawaysName' ,
                            'expanded' => false) )
                ->add('national_park', EntityType::class,
                      array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:NationalPark',
                            'choice_label' => 'dbParkName' ,
                            'expanded' => false) )
                ->add('sights', EntityType::class,
                      array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:Sights',
                            'choice_label' => 'dbSightsName' ,
                            'expanded' => false) )
                ->add('drinking', EntityType::class,
                        array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:Drinking',
                            'choice_label' => 'dbDrinkingName' ,
                            'expanded' => false) )
                ->add('eating', EntityType::class,
                        array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:Eating',
                            'choice_label' => 'dbEatingName' ,
                            'expanded' => false) )
                ->add('sleeping', EntityType::class,
                        array('placeholder' => 'Choose an option',
                            'required' => false,
                            'class' => 'AppBundle:Sleeping',
                            'choice_label' => 'dbSleepingName' ,
                            'expanded' => false) )
                ->add('Submit', SubmitType::class, array('attr' => array('class' => 'btn btn-success btn-group-justified')) )
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Images'
        ));
    }

    /**
     * {@inheritdoc}
     */

    public function getBlockPrefix()
    {
        return 'appbundle_images';
    }

}
