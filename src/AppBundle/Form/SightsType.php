<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SightsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dbSightsName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
            ->add('dbSightsInfo', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
            ->add('dbSightsDescription', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
            ->add('city', EntityType::class,
                array('placeholder' => 'Choose an option',
                      'required' => false,
                      'class' => 'AppBundle:City',
                      'choice_label' => 'dbCityName',
                      'expanded' => false) )
            ->add('national_park', EntityType::class,
                array('placeholder' => 'Choose an option',
                      'required' => false,
                      'class' => 'AppBundle:NationalPark',
                      'choice_label' => 'dbParkName',
                      'expanded' => false) )
            ->add('Submit', SubmitType::class,
                array('attr' => array('class' => 'btn btn-success btn-group-justified', 'style' => 'margin-bottom: 15px')) );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Sights'
        ));
    }
}
