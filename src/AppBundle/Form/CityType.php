<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dbCityName', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')) )
            ->add('dbCityInfo', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')))
            ->add('dbCityDescription', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px')))
            ->add('state', EntityType::class,
                  array('class' => 'AppBundle:State',
                        'choice_label' => 'dbStateName' ,
                        'expanded' => false), array('attr' => array('class' => 'selectpicker', 'data-style' => 'btn-info')) )
            ->add('Submit', SubmitType::class, array('attr' => array('class' => 'btn btn-success btn-group-justified', 'style' => 'margin-bottom: 15px')) );
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\City'
        ));
    }

}
