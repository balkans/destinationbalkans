<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Drinking;
use AppBundle\Form\DrinkingType;

class  AddDrinkingController extends Controller
{

        /**
         * @Route("admin/drinking/listing/", name="drinkingListing")
         */
        public function listDrinkingAction()
        {
            $drinks = $this->getDoctrine()
                ->getRepository('AppBundle:Drinking')
                ->findAll();

            return $this->render('crud/list/listingdrinking.html.twig', array(
                'drinks' => $drinks,
            ));
        }
        /**
         * @Route("admin/adddrinking/", name="addDrinking")
         */
        public function addDrinkingAction(Request $request)
        {
          $drink = new Drinking();
          $form = $this->createForm(DrinkingType::class, $drink);


          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $drinkname = $form['dbDrinkingName']->getData();
            $drinkinfo = $form['dbDrinkingInfo']->getData();
            $drinkdesc = $form['dbDrinkingDescription']->getData();
            $drinklctn = $form['dbDrinkingLocation']->getData();
            $drinkcity = $form['city']->getData();

            $drink->setDbDrinkingName($drinkname);
            $drink->setDbDrinkingInfo($drinkinfo);
            $drink->setDbDrinkingDescription($drinkdesc);
            $drink->setDbDrinkingLocation($drinklctn);
            $drink->setCity($drinkcity);

            $em = $this->getDoctrine()->getManager();
            $em->persist($drink);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/adddrinking.html.twig', array(
            'form' => $form->createView(),
          ));
        }

    /**
     * @Route("admin/drinking/{dbDrinkingName}", name="editDrinking")
     */
    public function editDrinkingAction($dbDrinkingName, Request $request)
    {
        $drink = $this->getDoctrine()
            ->getRepository('AppBundle:Drinking')
            ->findOneByDbDrinkingName($dbDrinkingName);

        $drink->setDbDrinkingName($drink->getDbDrinkingName());
        $drink->setDbDrinkingInfo($drink->getDbDrinkingInfo());
        $drink->setDbDrinkingWeb($drink->getDbDrinkingWeb());
        $drink->setCity($drink->getCity());

        $form = $this->createForm(DrinkingType::class, $drink);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $drinkname = $form['dbDrinkingName']->getData();
            $drinkinfo = $form['dbDrinkingInfo']->getData();
            $drinkdesc = $form['dbDrinkingWeb']->getData();
            $drinklctn = $form['dbDrinkingLocation']->getData();
            $drinkcity = $form['city']->getData();

            $em = $this->getDoctrine()->getManager();
            $drink = $em->getRepository('AppBundle:Drinking')
                ->findOneByDbDrinkingName($dbDrinkingName);

            $drink->setDbDrinkingName($drinkname);
            $drink->setDbDrinkingInfo($drinkinfo);
            $drink->setDbDrinkingWeb($drinkdesc);
            $drink->setDbDrinkingLocation($drinklctn);
            $drink->setCity($drinkcity);

            $em->flush();
        }
        return $this->render('crud/edit/drinkingedit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

        /**
         * @Route("admin/deletedrinking/{id}", name="deleteDrinking")
         */
        public function deleteDrinkingAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $drink = $em->getRepository('AppBundle:Drinking')->find($id);

            if (!$drink) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $em->remove($drink);
            $em->flush();

            return $this->redirectToRoute('stateEntries');
        }
}
