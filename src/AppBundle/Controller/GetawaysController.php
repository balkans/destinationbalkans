<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\State;
use AppBundle\Entity\Getaways;
use AppBundle\Entity\Images;

class GetawaysController extends Controller
{
      /**
      * @Route("{dbStateName}/getaway/{dbGetawaysName}/", name="selectedGetaway")
      */
      public function getawaysAction($dbGetawaysName)
      {
      $getaway = $this->getDoctrine()
          ->getRepository('AppBundle:Getaways')
          ->findOneByDbGetawaysName($dbGetawaysName);

      $photo = $getaway->getImages();
      return $this->render('singlerecords/getaway.html.php', array(
          'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
          'getaway' => $getaway,
          'photo' => $photo,
          ));
      }
      /**
      * @Route("{dbStateName}/allgetaways/", name="allGetaways")
      */
      public function allgetawaysAction($dbStateName)
      {
        $selstate = $this->getDoctrine()
            ->getRepository('AppBundle:State')
            ->findGetawaysImages($dbStateName);

      return $this->render('allrecords/allgetaways.html.php', array(
          'selstate' => $selstate,
          ));
      }
}
