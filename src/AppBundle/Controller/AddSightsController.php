<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Sights;
use AppBundle\Form\SightsType;

class  AddSightsController extends Controller
{

        /**
         * @Route("admin/sights/listing/", name="listSights")
         */
        public function listSightsAction()
        {
            $sights = $this->getDoctrine()
                ->getRepository('AppBundle:Sights')
                ->findAll();

            return $this->render('crud/list/listingsights.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
                'sights' => $sights
            ));
        }

        /**
         * @Route("admin/addsight/", name="addSight")
         */
        public function addsightsAction(Request $request)
        {
          $sg = new Sights();
          $form = $this->createForm(SightsType::class, $sg);


          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $sgname = $form['dbSightsName']->getData();
            $sginfo = $form['dbSightsInfo']->getData();
            $sgdesc = $form['dbSightsDescription']->getData();
            $sgcity = $form['city']->getData();


            $sg->setDbSightsName($sgname);
            $sg->setDbSightsInfo($sginfo);
            $sg->setDbSightsDescription($sgdesc);
            $sg->setCity($sgcity);

            $em = $this->getDoctrine()->getManager();
            $em->persist($sg);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/addsight.html.twig', array(
            'form' => $form->createView(),
          ));
        }

        /**
         * @Route("admin/editsights/{dbSightsName}", name="editSight")
         */
        public function editSightAction($dbSightsName, Request $request)
        {
            $sight = $this->getDoctrine()
                ->getRepository('AppBundle:Sights')
                ->findOneByDbSightsName($dbSightsName);

            $sight->setDbSightsName($sight->getDbSightsName());
            $sight->setDbSightsInfo($sight->getDbSightsInfo());
            $sight->setDbSightsDescription($sight->getDbSightsDescription());
            $sight->setCity($sight->getCity());

            $form = $this->createForm(SightsType::class, $sight);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid())
            {
                $sname = $form['dbSightsName']->getData();
                $sinfo = $form['dbSightsInfo']->getData();
                $sdesc = $form['dbSightsDescription']->getData();
                $scity = $form['city']->getData();

                $em = $this->getDoctrine()->getManager();
                $sight = $em->getRepository('AppBundle:Sights')
                    ->findOneByDbSightsName($dbSightsName);

                $sight->setDbSightsName($sname);
                $sight->setDbSightsInfo($sinfo);
                $sight->setDbSightsDescription($sdesc);
                $sight->setCity($scity);

                $em->flush();
            }

            return $this->render('crud/edit/sightsedit.html.twig', array(
                'form' => $form->createView(),
            ));
        }

        /**
         * @Route("admin/deletesight/{id}", name="deleteSight")
         */
        public function deleteSightsAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $sight = $em->getRepository('AppBundle:Sights')->find($id);

            if (!$sight) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $em->remove($sight);
            $em->flush();

            return $this->redirectToRoute('stateEntries');
        }
}
