<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\State;
use AppBundle\Entity\City;

class CitiesController extends Controller
{

      /**
      * @Route("{dbStateName}/city/{dbCityName}/", name="selectedCity")
      */
      public function cityAction($dbCityName)
      {
          $selcity = $this->getDoctrine()
              ->getRepository('AppBundle:City')
              ->findOneByDbCityName($dbCityName);
          $i = $selcity->getImages();

          $sight = $this->getDoctrine()
              ->getRepository('AppBundle:City')
              ->findCitySightsImages($dbCityName);

          $eat = $this->getDoctrine()
              ->getRepository('AppBundle:City')
              ->findEatingProperties($dbCityName);

          $drink = $this->getDoctrine()
              ->getRepository('AppBundle:City')
              ->findDrinkingProperties($dbCityName);

          $sleep = $this->getDoctrine()
              ->getRepository('AppBundle:City')
              ->findSleepingProperties($dbCityName);

      return $this->render('singlerecords/city.html.php', array(
          'selcity' => $selcity,
          'i' => $i,
          'sight' => $sight,
          'eat' => $eat,
          'drink' => $drink,
          'sleep' => $sleep,
          ));
      }

      /**
      * @Route("{dbStateName}/allcities/", name="allCities")
      */
      public function allcitiesAction($dbStateName)
      {

      $selcitystate = $this->getDoctrine()
            ->getRepository('AppBundle:State')
            ->findCityImages($dbStateName); //custom query method in StateRepository.php
      $randomstatephoto = $selcitystate[mt_rand(0, count($selcitystate) - 1)];

      return $this->render('allrecords/allcities.html.php', array(
          'selcitystate' => $selcitystate,
          'randomstatephoto' => $randomstatephoto,
          ));
      }

}
