<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Sleeping;
use AppBundle\Form\SleepingType;

class  AddSleepingController extends Controller
{
        /**
         * @Route("admin/sleeping/listing/", name="listSleeping")
         */
        public function listSlepingAction()
        {
            $sleeps = $this->getDoctrine()
                ->getRepository('AppBundle:Sleeping')
                ->findAll();

            return $this->render('crud/list/listingsleeping.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
                'sleeps' => $sleeps
            ));
        }

        /**
         * @Route("admin/addsleeping/", name="addSleeping")
         */
        public function addSleepingAction(Request $request)
        {
          $sleep = new Sleeping();
          $form = $this->createForm(SleepingType::class, $sleep);


          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $sn = $form['dbSleepingName']->getData();
            $si = $form['dbSleepingInfo']->getData();
            $sw = $form['dbSleepingWeb']->getData();
            $sl = $form['dbSleepingLocation']->getData();
            $sc = $form['city']->getData();

            $sleep->setDbSleepingName($sn);
            $sleep->setDbSleepingInfo($si);
            $sleep->setDbSleepingWeb($sw);
            $sleep->setDbSleepingLocation($sl);
            $sleep->setCity($sc);

            $em = $this->getDoctrine()->getManager();
            $em->persist($sleep);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/addsleeping.html.twig', array(
            'form' => $form->createView(),
          ));
        }

    /**
     * @Route("admin/edit/sleeping/{dbSleepingName}", name="editSleeping")
     */
    public function editSleepingAction($dbSleepingName, Request $request)
    {
        $sleep = $this->getDoctrine()
            ->getRepository('AppBundle:Sleeping')
            ->findOneByDbSleepingName($dbSleepingName);

        $sleep->setDbSleepingName($sleep->getDbSleepingName());
        $sleep->setDbSleepingInfo($sleep->getDbSleepingInfo());
        $sleep->setDbSleepingWeb($sleep->getDbSleepingWeb());
        $sleep->setDbSleepingLocation($sleep->getDbSleepingLocation());
        $sleep->setCity($sleep->getCity());

        $form = $this->createForm(SleepingType::class, $sleep);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $sleepname = $form['dbSleepingName']->getData();
            $sleepinfo = $form['dbSleepingInfo']->getData();
            $sleepwebb = $form['dbSleepingWeb']->getData();
            $sleeplctn = $form['dbSleepingLocation']->getData();
            $sleepcity = $form['city']->getData();

            $em = $this->getDoctrine()->getManager();
            $sleep = $em->getRepository('AppBundle:Sleeping')
                ->findOneByDbSleepingName($dbSleepingName);

            $sleep->setDbSleepingName($sleepname);
            $sleep->setDbSleepingInfo($sleepinfo);
            $sleep->setDbSleepingWeb($sleepwebb);
            $sleep->setDbSleepingLocation($sleeplctn);
            $sleep->setCity($sleepcity);

            $em->flush();
        }
        return $this->render('crud/edit/sleepingedit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route("admin/deletesleeping/{id}", name="deleteSleeping")
     */
    public function deleteSleepingAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $sleep = $em->getRepository('AppBundle:Sleeping')
                    ->find($id);

        if (!$sleep) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $em->remove($sleep);
        $em->flush();

        return $this->redirectToRoute('stateEntries');
    }
}
