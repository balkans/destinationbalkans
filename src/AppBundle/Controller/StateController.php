<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\State;
use AppBundle\Entity\City;
use AppBundle\Entity\NationalPark;
use AppBundle\Entity\Getaways;

class StateController extends Controller
{

  /**
  * @Route("{dbStateName}/", name="selectedState")
  */
  public function stateAction($dbStateName)
  {

      $city = $this->getDoctrine()
          ->getRepository('AppBundle:State')
          ->randomCityAction($dbStateName);

      $randomcity = $city[mt_rand(0, count($city) - 1)];

      $getaway = $this->getDoctrine()
          ->getRepository('AppBundle:State')
          ->randomGetawaysAction($dbStateName);
      $randomgetaway = $getaway[mt_rand(0, count($getaway) - 1)];

      $np = $this->getDoctrine()
          ->getRepository('AppBundle:State')
          ->randomParkAction($dbStateName);
      $randomnp = $np[mt_rand(0, count($np) - 1)];

      $state = $this->getDoctrine()
          ->getRepository('AppBundle:State')
          ->randomStateAction($dbStateName);
      $randomstatephoto = $state[mt_rand(0, count($state) - 1)];


      return $this->render('homepage/landing.html.php', array(
          'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
          'randomstatephoto' => $randomstatephoto,
          'randomcity'=> $randomcity,
          'randomgetaway' => $randomgetaway,
          'randomnp' => $randomnp,
      ));
  }
}
