<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Getaways;
use AppBundle\Form\GetawaysType;

class  AddGetawaysController extends Controller
{

        /**
         * @Route("admin/getaways/listing/", name="listGetaways")
         */
        public function listGetawaysAction()
        {
            $getaways = $this->getDoctrine()
                ->getRepository('AppBundle:Getaways')
                ->findAll();

            return $this->render('crud/list/listinggetaways.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
                'getaways' => $getaways
            ));
        }

        /**
         * @Route("admin/addgetaway/", name="addGetaway")
         */
        public function addgetawayAction(Request $request)
        {
          $gt = new Getaways();
          $form = $this->createForm(GetawaysType::class, $gt);


          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $gtname = $form['dbGetawaysName']->getData();
            $gtinfo = $form['dbGetawaysInfo']->getData();
            $gtdesc = $form['dbGetawaysDescription']->getData();
            $gtstate = $form['state']->getData();


            $gt->setDbGetawaysName($gtname);
            $gt->setDbGetawaysInfo($gtinfo);
            $gt->setDbGetawaysDescription($gtdesc);
            $gt->setState($gtstate);

            $em = $this->getDoctrine()->getManager();
            $em->persist($gt);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/addgetaway.html.twig', array(
            'form' => $form->createView(),
          ));
        }
      /**
       * @Route("admin/edit/getaway/{dbGetawaysName}", name="editGetaway")
       */
       public function editgetawaysAction($dbGetawaysName, Request $request)
       {
         $getaway = $this->getDoctrine()
                  ->getRepository('AppBundle:Getaways')
                  ->findOneByDbGetawaysName($dbGetawaysName);

          $getaway->setDbGetawaysName($getaway->getDbGetawaysName());
          $getaway->setDbGetawaysInfo($getaway->getDbGetawaysInfo());
          $getaway->setDbGetawaysDescription($getaway->getDbGetawaysDescription());
          $getaway->setState($getaway->getState());

          $form = $this->createForm(GetawaysType::class, $getaway);
          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid())
          {
            $gtname = $form['dbGetawaysName']->getData();
            $gtinfo = $form['dbGetawaysInfo']->getData();
            $gtdesc = $form['dbGetawaysDescription']->getData();
            $gtstate = $form['state']->getData();

            $em = $this->getDoctrine()->getManager();
            $getaway = $em->getRepository('AppBundle:Getaways')
                          ->findOneByDbGetawaysName($dbGetawaysName);

            $getaway->setDbGetawaysName($gtname);
            $getaway->setDbGetawaysInfo($gtinfo);
            $getaway->setDbGetawaysDescription($gtdesc);
            $getaway->setState($gtstate);

            $em->flush();
          }
          return $this->render('crud/edit/getawayedit.html.twig', array(
            'form' => $form->createView(),
          ));
      }

        /**
         * @Route("admin/deletegetaway/{id}", name="deleteGetaway")
         */
        public function deleteGetawaysAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $getaway = $em->getRepository('AppBundle:Getaways')->find($id);

            if (!$getaway) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $em->remove($getaway);
            $em->flush();

            return $this->redirectToRoute('stateEntries');
        }
}
