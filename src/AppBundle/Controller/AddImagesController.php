<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Images;
use AppBundle\Form\ImagesType;

class  AddImagesController extends Controller {

        /**
         * @Route("admin/images/listingimages/", name="listImages")
         */
        public function listImagesAction(Request $request)
        {
          $images = $this->getDoctrine()
                    ->getRepository('AppBundle:Images')
                    ->findAll();

            return $this->render('crud/list/listingimages.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
                'images' => $images
            ));
        }

        /**
         * @Route("admin/addimages/", name="addImages")
         */
        public function addImagesAction(Request $request)
        {
          $i = new Images();
          $form = $this->createForm(ImagesType::class, $i);


          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $i->getDbImageFile();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('images_directory'),
                $fileName
            );


            $iname = $form['dbImageName']->getData();
            $ifp = $form['dbFilenameProperty']->getData();
            $itime = $form['updatedAt']->getData();
            $istate = $form['state']->getData();
            $icity = $form['city']->getData();
            $igetaway = $form['getaways']->getData();
            $inp = $form['national_park']->getData();
            $isight = $form['sights']->getData();
            $idrink = $form['drinking']->getData();
            $ieat = $form['eating']->getData();
            $isleep = $form['sleeping']->getData();

            $i->setDbImageFile($fileName);
            $i->setDbImageName($iname);
            $i->setDbFilenameProperty($ifp);
            $i->setUpdatedAt($itime);
            $i->setState($istate);
            $i->setCity($icity);
            $i->setGetaways($igetaway);
            $i->setNationalPark($inp);
            $i->setSights($isight);
            $i->setDrinking($idrink);
            $i->setEating($ieat);
            $i->setSleeping($isleep);

            $em = $this->getDoctrine()->getManager();
            $em->persist($i);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/addimage.html.twig', array(
            'form' => $form->createView(),
          ));
        }

        /**
         * @Route("admin/editimages/{dbImageFile}", name="addPhoto")
         */
        public function editimagesAction($dbImageFile, Request $request)
        {
          $i = $this->getDoctrine()
                 ->getRepository('AppBundle:City')
                 ->findOneByDbCityName($dbImageFile);

           $i->setDbImageFile($i->getDbImageFile());
           $i->setDbImageName($i->getDbImageName());
           $i->setDbFilenameProperty($i->getDbFilenameProperty());
           $i->setUpdatedAt($i->getUpdatedAt());
           $i->setState($i->getState());
           $i->setCity($i->getCity());
           $i->setGetaways($i->getGetaways());
           $i->setNationalPark($i->getNationalPark());
           $i->setSights($i->getSights());

           $form = $this->createForm(ImageType::class, $i);
           $form->handleRequest($request);

           if($form->isSubmitted() && $form->isValid())
           {
             /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
             $file = $i->getDbImageFile();
             $fileName = md5(uniqid()).'.'.$file->guessExtension();
             $file->move(
                 $this->getParameter('images_directory'),
                 $fileName
             );
             $em = $this->getDoctrine()->getManager();
             $i = $em->getRepository('AppBundle:Images')
                       ->findOneByDbCityName($dbImageFile);

             $i->setDbImageFile($fileName);
             $i->setDbImageName($iname);
             $i->setDbFilenameProperty($ifp);
             $i->setUpdatedAt($itime);
             $i->setState($istate);
             $i->setCity($icity);
             $i->setGetaways($igetaway);
             $i->setNationalPark($inp);
             $i->setSights($isight);

             $em->flush();
           }
           return $this->render('crud/edit/imgedit.html.twig', array(
             'form' => $form->createView(),
           ));
        }

        /**
         * @Route("admin/deleteimage/{id}", name="deleteImage")
         */
        public function deleteImagesAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $image = $em->getRepository('AppBundle:Images')
                        ->find($id);

            if (!$image) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $em->remove($image);
            $em->flush();

            return $this->redirectToRoute('stateEntries');
        }
}
