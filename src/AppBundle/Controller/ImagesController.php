<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Images;

class ImagesController extends Controller
{
    /**
     * @Route("/images", name="images")
     */
    public function listphotosAction()
    {
        $images = $this->getDoctrine()
        ->getRepository('AppBundle:Images')
        ->findAll();
        return $this->render('homepage/images.html.php', array(
            'images' => $images
        ));
    }
}
