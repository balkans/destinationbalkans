<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\State;
use AppBundle\Entity\City;
use AppBundle\Entity\NationalPark;
use AppBundle\Entity\Getaways;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function listAction()
    {
        $states = $this->getDoctrine()
        ->getRepository('AppBundle:State')
        ->findAll();

        return $this->render('homepage/index.html.php', array(
            'state' => $states
        ));
    }
}
