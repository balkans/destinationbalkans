<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\State;
use AppBundle\Entity\City;
use AppBundle\Entity\NationalPark;
use AppBundle\Entity\Getaways;
use AppBundle\Form\StateType;

class AddStateController extends Controller
{
        /**
        * @Route("/admin/list", name="stateEntries")
        */
        public function listAction()
        {
            $states = $this->getDoctrine()
                ->getRepository('AppBundle:State')
                ->findAll();

            return $this->render('crud/homepagelist.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
                'states' => $states,
            ));
        }
       /**
       * @Route("/admin/listing/{dbStateName}", name="allEntries")
       */
       public function listingsAction($dbStateName)
       {
           $stt = $this->getDoctrine()
               ->getRepository('AppBundle:State')
               ->findOneByDbStateName($dbStateName);

           $ct = $stt->getCity();
           $np = $stt->getNationalPark();
           $gw = $stt->getGetaways();

           return $this->render('crud/list/listallperstate.html.twig', array(
               'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
               'stt' => $stt,
               'ct' => $ct,
               'np' => $np,
               'gw' => $gw,
           ));
       }

      /**
       * @Route("admin/addcountry/", name="addState")
       */
       public function addstateAction(Request $request)
       {
         $state = new State();
         $state->setDbStateName();
         $state->setDbStateInfo();
         $state->setDbStateDescription();

         $em = $this->getDoctrine()->getManager();
         $em->persist($state);
         $em->flush();

         return $this->render('crud/add/addstate.html.twig');
       }

       /**
        * @Route("admin/statesedit/{dbStateName}", name="editState")
        */
        public function editAction($dbStateName, Request $request)
        {
          $state = $this->getDoctrine()
              ->getRepository('AppBundle:State')
              ->findOneByDbStateName($dbStateName);

          $state->setDbStateName($state->getDbStateName());
          $state->setDbStateInfo($state->getDbStateInfo());
          $state->setDbStateDescription($state->getDbStateDescription());

          $form = $this->createForm(StateType::class, $state);
          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid())
          {
            $sname = $form['dbStateName']->getData();
            $sinfo = $form['dbStateInfo']->getData();
            $sdesc = $form['dbStateDescription']->getData();

            $em = $this->getDoctrine()->getManager();
            $state = $em->getRepository('AppBundle:State')
                      ->findOneByDbStateName($dbStateName);

            $state->setDbStateName($sname);
            $state->setDbStateInfo($sinfo);
            $state->setDbStateDescription($sdesc);

            $em->flush();

          }
          return $this->render('crud/edit/statesedit.html.twig',
                  array(
                    'form' => $form->createView()
                  ));
        }

}
