<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AdminUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Appbundle\UserAdmin;

class SecurityController extends Controller
{
    /**
     * @Route("/admin/login", name="adminLogin")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }


    /**
     * @Route("/admin/adduser/", name="addUser")
     */
    public function addUserAction()
    {
        $user = new AdminUser();
        $user->setDbUserName('georgesuperadmin');
        $user->setDbUserPassword('1234');
        $user->setDbUserStatus('ACTIVE');

        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($user);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return new Response('Saved new user with name '.$user->getDbUserName());
    }


}
