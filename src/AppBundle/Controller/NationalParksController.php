<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\State;
use AppBundle\Entity\City;
use AppBundle\Entity\NationalPark;
use AppBundle\Entity\Getaways;
use AppBundle\Entity\Images;

class NationalParksController extends Controller
{

    /**
    * @Route("{dbStateName}/nationalpark/{dbParkName}", name="selectedNationalPark")
    */
    public function nationalparkAction($dbParkName)
    {
         $national_parks = $this->getDoctrine()
            ->getRepository('AppBundle:NationalPark')
            ->findOneByDbParkName($dbParkName);

         $sight = $this->getDoctrine()
             ->getRepository('AppBundle:NationalPark')
             ->findNationalParkSightsImages($dbParkName);

        $photo = $national_parks->getImages();
        $state = $national_parks->getState();

        return $this->render('singlerecords/park.html.php', array(
        'national_parks' => $national_parks,
        'photo' => $photo,
        'state' => $state,
        'sight' => $sight,
        ));
    }

    /**
    * @Route("{dbStateName}/allnationalparks/", name="allNationalParks")
    */
    public function allnparksAction($dbStateName)
    {
      $selstate = $this->getDoctrine()
          ->getRepository('AppBundle:State')
          ->findParkImages($dbStateName);

    return $this->render('allrecords/allparks.html.php', array(  
        'selstate' => $selstate,
        ));
    }
}
