<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Eating;
use AppBundle\Form\EatingType;

class  AddEatingController extends Controller
{
        /**
         * @Route("admin/eating/listing/", name="listEating")
         */
        public function listEatingAction()
        {
            $eats = $this->getDoctrine()
                ->getRepository('AppBundle:Eating')
                ->findAll();

            return $this->render('crud/list/listingeating.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
                'eats' => $eats
            ));
        }

        /**
         * @Route("admin/addeating/", name="addEating")
         */
        public function addEatingAction(Request $request)
        {
          $eat = new Eating();
          $form = $this->createForm(EatingType::class, $eat);


          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $ename = $form['dbEatingName']->getData();
            $einfo = $form['dbEatingInfo']->getData();
            $edesc = $form['dbEatingWeb']->getData();
            $ecity = $form['city']->getData();

            $eat->setDbEatingName($ename);
            $eat->setDbEatingInfo($einfo);
            $eat->setDbEatingWeb($edesc);
            $eat->setCity($ecity);

            $em = $this->getDoctrine()->getManager();
            $em->persist($eat);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/addeating.html.twig', array(
            'form' => $form->createView(),
          ));
        }

      /**
       * @Route("admin/edit/eating/{dbEatingName}", name="editEating")
       */
       public function editEatingAction($dbEatingName, Request $request)
       {
         $eat = $this->getDoctrine()
                  ->getRepository('AppBundle:Eating')
                  ->findOneByDbEatingName($dbEatingName);

           $eat->setDbEatingName($eat->getDbEatingName());
           $eat->setDbEatingInfo($eat->getDbEatingInfo());
           $eat->setDbEatingWeb($eat->getDbEatingWeb());
           $eat->setCity($eat->getCity());

          $form = $this->createForm(EatingType::class, $eat);
          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid())
          {
            $eatname = $form['dbEatingName']->getData();
            $eatinfo = $form['dbEatingInfo']->getData();
            $eatdesc = $form['dbEatingWeb']->getData();
            $eatlctn = $form['dbEatingLocation']->getData();
            $eatcity = $form['city']->getData();

            $em = $this->getDoctrine()->getManager();
            $eat = $em->getRepository('AppBundle:Eating')
                          ->findOneByDbEatingName($dbEatingName);

            $eat->setDbEatingName($eatname);
            $eat->setDbEatingInfo($eatinfo);
            $eat->setDbEatingWeb($eatdesc);
            $eat->setDbEatingLocation($eatlctn);
            $eat->setCity($eatcity);

            $em->flush();
          }
          return $this->render('crud/edit/eatingedit.html.twig', array(
            'form' => $form->createView(),
          ));
      }

        /**
         * @Route("admin/deleteeating/{id}", name="deleteEating")
         */
        public function deleteEatingAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $eat = $em->getRepository('AppBundle:Eating')->find($id);

            if (!$eat) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $em->remove($eat);
            $em->flush();

            return $this->redirectToRoute('stateEntries');
        }
}
