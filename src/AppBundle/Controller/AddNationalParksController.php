<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\NationalPark;
use AppBundle\Form\NationalParkType;

class  AddNationalParksController extends Controller
{
        /**
         * @Route("admin/nationalpark/listing/", name="listNationalParks")
         */
        public function listNationalParksAction()
        {
            $nparks = $this->getDoctrine()
                ->getRepository('AppBundle:NationalPark')
                ->findAll();

            return $this->render('crud/list/listingnationalparks.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
                'nparks' => $nparks
            ));
        }

        /**
         * @Route("admin/addnpark/", name="addNPark")
         */
        public function addnparksAction(Request $request)
        {
          $np = new NationalPark();
          $form = $this->createForm(NationalParkType::class, $np);


          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid())
          {
            $npname = $form['dbParkName']->getData();
            $npinfo = $form['dbParkInfo']->getData();
            $npdesc = $form['dbParkDescription']->getData();
            $npstate = $form['state']->getData();


            $np->setDbParkName($npname);
            $np->setDbParkInfo($npinfo);
            $np->setDbParkDescription($npdesc);
            $np->setState($npstate);

            $em = $this->getDoctrine()->getManager();
            $em->persist($np);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/addnpark.html.twig', array(
            'form' => $form->createView(),
          ));
        }

      /**
       * @Route("admin/editnationalpark/{dbParkName}", name="editNPark")
       */
       public function editparkAction($dbParkName, Request $request)
       {
         $np = $this->getDoctrine()
              ->getRepository('AppBundle:NationalPark')
              ->findOneByDbParkName($dbParkName);

          $np->setDbParkName($np->getDbParkName());
          $np->setDbParkInfo($np->getDbParkInfo());
          $np->setDbParkDescription($np->getDbParkDescription());
          $np->setState($np->getState());

          $form = $this->createForm(NationalParkType::class, $np);
          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid())
          {
            $npname = $form['dbParkName']->getData();
            $npinfo = $form['dbParkInfo']->getData();
            $npdesc = $form['dbParkDescription']->getData();
            $npstate = $form['state']->getData();

            $em = $this->getDoctrine()->getManager();
            $np = $em->getRepository('AppBundle:NationalPark')
                     ->findOneByDbParkName($dbParkName);

            $np->setDbParkName($npname);
            $np->setDbParkInfo($npinfo);
            $np->setDbParkDescription($npdesc);
            $np->setState($npstate);

            $em->flush();
          }
          return $this->render('crud/edit/parkedit.html.twig', array(
            'form' => $form->createView(),
          ));
       }

        /**
         * @Route("admin/deletenationalpark/{id}", name="deleteNationalPark")
         */
        public function deleteNationalParksAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $np = $em->getRepository('AppBundle:NationalPark')->find($id);

            if (!$np) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $em->remove($np);
            $em->flush();

            return $this->redirectToRoute('stateEntries');
        }
}
