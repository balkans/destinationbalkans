<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\City;
use AppBundle\Form\CityType;

class  AddCitiesController extends Controller
{

        /**
         * @Route("admin/cities/listingcities/", name="citiesListing")
         */
        public function listCitiesAction()
        {
            $cities = $this->getDoctrine()
                ->getRepository('AppBundle:City')
                ->findAll();

            return $this->render('crud/list/listingcities.html.twig', array(
                'cities' => $cities,
            ));
        }

        /**
         * @Route("admin/addcity/", name="addCity")
         */
        public function addCitiesAction(Request $request)
        {
          $city = new City();
          $form = $this->createForm(CityType::class, $city);

          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid()){
            $cityname = $form['dbCityName']->getData();
            $cityinfo = $form['dbCityInfo']->getData();
            $citydesc = $form['dbCityDescription']->getData();
            $citystate = $form['state']->getData();


            $city->setDbCityName($cityname);
            $city->setDbCityInfo($cityinfo);
            $city->setDbCityDescription($citydesc);
            $city->setState($citystate);

            $em = $this->getDoctrine()->getManager();
            $em->persist($city);
            $em->flush();

            $this->addFlash(
              'Message',
              'Property Added'
            );
          }
          return $this->render('crud/add/addcity.html.twig', array(
            'form' => $form->createView(),
          ));
        }

        /**
         * @Route("admin/city/{dbCityName}", name="editCity")
         */
        public function editCityAction($dbCityName, Request $request)
        {
          $city = $this->getDoctrine()
                 ->getRepository('AppBundle:City')
                 ->findOneByDbCityName($dbCityName);

          $city->setDbCityName($city->getDbCityName());
          $city->setDbCityInfo($city->getDbCityInfo());
          $city->setDbCityDescription($city->getDbCityDescription());
          $city->setState($city->getState());

          $form = $this->createForm(CityType::class, $city);
          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid())
          {
            $cityname = $form['dbCityName']->getData();
            $cityinfo = $form['dbCityInfo']->getData();
            $citydesc = $form['dbCityDescription']->getData();
            $citystate = $form['state']->getData();

            $em = $this->getDoctrine()->getManager();
            $city = $em->getRepository('AppBundle:City')
                      ->findOneByDbCityName($dbCityName);

            $city->setDbCityName($cityname);
            $city->setDbCityInfo($cityinfo);
            $city->setDbCityDescription($citydesc);
            $city->setState($citystate);

            $em->flush();
          }

            return $this->render('crud/edit/cityedit.html.twig', array(
              'form' => $form->createView(),
            ));
        }

        /**
         * @Route("admin/deletecity/{id}", name="deleteCity")
         */
        public function deleteCitiesAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $city = $em->getRepository('AppBundle:City')->find($id);

            if (!$city) {
                throw $this->createNotFoundException(
                    'No product found for id '.$id
                );
            }

            $em->remove($city);
            $em->flush();

            return $this->redirectToRoute('stateEntries');
        }
}
