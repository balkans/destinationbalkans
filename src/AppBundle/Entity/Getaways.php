<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Getaways
 */
class Getaways
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbGetawaysName;

    /**
     * @var string
     */
    private $dbGetawaysInfo;

    /**
     * @var string
     */
    private $dbGetawaysDescription;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbGetawaysName
     *
     * @param string $dbGetawaysName
     * @return Getaways
     */
    public function setDbGetawaysName($dbGetawaysName)
    {
        $this->dbGetawaysName = $dbGetawaysName;

        return $this;
    }

    /**
     * Get dbGetawaysName
     *
     * @return string
     */
    public function getDbGetawaysName()
    {
        return $this->dbGetawaysName;
    }

    /**
     * Set dbGetawaysInfo
     *
     * @param string $dbGetawaysInfo
     * @return Getaways
     */
    public function setDbGetawaysInfo($dbGetawaysInfo)
    {
        $this->dbGetawaysInfo = $dbGetawaysInfo;

        return $this;
    }

    /**
     * Get dbGetawaysInfo
     *
     * @return string
     */
    public function getDbGetawaysInfo()
    {
        return $this->dbGetawaysInfo;
    }

    /**
     * Set dbGetawaysDescription
     *
     * @param string $dbGetawaysDescription
     * @return Getaways
     */
    public function setDbGetawaysDescription($dbGetawaysDescription)
    {
        $this->dbGetawaysDescription = $dbGetawaysDescription;

        return $this;
    }

    /**
     * Get dbGetawaysDescription
     *
     * @return string
     */
    public function getDbGetawaysDescription()
    {
        return $this->dbGetawaysDescription;
    }
    /**
     * @var \AppBundle\Entity\State
     */
    private $state;


    /**
     * Set state
     *
     * @param \AppBundle\Entity\State $state
     * @return Getaways
     */
    public function setState(\AppBundle\Entity\State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \AppBundle\Entity\State
     */
    public function getState()
    {
        return $this->state;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Getaways
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $drinking;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eating;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sleeping;


    /**
     * Add drinking
     *
     * @param \AppBundle\Entity\Drinking $drinking
     *
     * @return Getaways
     */
    public function addDrinking(\AppBundle\Entity\Drinking $drinking)
    {
        $this->drinking[] = $drinking;

        return $this;
    }

    /**
     * Remove drinking
     *
     * @param \AppBundle\Entity\Drinking $drinking
     */
    public function removeDrinking(\AppBundle\Entity\Drinking $drinking)
    {
        $this->drinking->removeElement($drinking);
    }

    /**
     * Get drinking
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDrinking()
    {
        return $this->drinking;
    }

    /**
     * Add eating
     *
     * @param \AppBundle\Entity\Eating $eating
     *
     * @return Getaways
     */
    public function addEating(\AppBundle\Entity\Eating $eating)
    {
        $this->eating[] = $eating;

        return $this;
    }

    /**
     * Remove eating
     *
     * @param \AppBundle\Entity\Eating $eating
     */
    public function removeEating(\AppBundle\Entity\Eating $eating)
    {
        $this->eating->removeElement($eating);
    }

    /**
     * Get eating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEating()
    {
        return $this->eating;
    }

    /**
     * Add sleeping
     *
     * @param \AppBundle\Entity\Sleeping $sleeping
     *
     * @return Getaways
     */
    public function addSleeping(\AppBundle\Entity\Sleeping $sleeping)
    {
        $this->sleeping[] = $sleeping;

        return $this;
    }

    /**
     * Remove sleeping
     *
     * @param \AppBundle\Entity\Sleeping $sleeping
     */
    public function removeSleeping(\AppBundle\Entity\Sleeping $sleeping)
    {
        $this->sleeping->removeElement($sleeping);
    }

    /**
     * Get sleeping
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSleeping()
    {
        return $this->sleeping;
    }
}
