<?php

namespace AppBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;


/**
 * Images
 */
class Images
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbImageFile;

    /**
     * @var string
     */
    private $dbImageName;

    /**
     * @var string
     */
    private $dbFilenameProperty;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbImageFile
     *
     * @param string $dbImageFile
     *
     * @return Images
     */
    public function setDbImageFile($dbImageFile)
    {
        $this->dbImageFile = $dbImageFile;

        return $this;
    }

    /**
     * Get dbImageFile
     *
     * @return string
     */
    public function getDbImageFile()
    {
        return $this->dbImageFile;
    }

    /**
     * Set dbImageName
     *
     * @param string $dbImageName
     *
     * @return Images
     */
    public function setDbImageName($dbImageName)
    {
        $this->dbImageName = $dbImageName;

        return $this;
    }

    /**
     * Get dbImageName
     *
     * @return string
     */
    public function getDbImageName()
    {
        return $this->dbImageName;
    }

    /**
     * Set dbFilenameProperty
     *
     * @param string $dbFilenameProperty
     *
     * @return Images
     */
    public function setDbFilenameProperty($dbFilenameProperty)
    {
        $this->dbFilenameProperty = $dbFilenameProperty;

        return $this;
    }

    /**
     * Get dbFilenameProperty
     *
     * @return string
     */
    public function getDbFilenameProperty()
    {
        return $this->dbFilenameProperty;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Images
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var \AppBundle\Entity\City
     */
    private $city;

    /**
     * @var \AppBundle\Entity\State
     */
    private $state;

    /**
     * @var \AppBundle\Entity\Getaways
     */
    private $getaways;

    /**
     * @var \AppBundle\Entity\NationalPark
     */
    private $national_park;

    /**
     * @var \AppBundle\Entity\Sights
     */
    private $sights;


    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Images
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param \AppBundle\Entity\State $state
     *
     * @return Images
     */
    public function setState(\AppBundle\Entity\State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \AppBundle\Entity\State
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set getaways
     *
     * @param \AppBundle\Entity\Getaways $getaways
     *
     * @return Images
     */
    public function setGetaways(\AppBundle\Entity\Getaways $getaways = null)
    {
        $this->getaways = $getaways;

        return $this;
    }

    /**
     * Get getaways
     *
     * @return \AppBundle\Entity\Getaways
     */
    public function getGetaways()
    {
        return $this->getaways;
    }

    /**
     * Set nationalPark
     *
     * @param \AppBundle\Entity\NationalPark $nationalPark
     *
     * @return Images
     */
    public function setNationalPark(\AppBundle\Entity\NationalPark $nationalPark = null)
    {
        $this->national_park = $nationalPark;

        return $this;
    }

    /**
     * Get nationalPark
     *
     * @return \AppBundle\Entity\NationalPark
     */
    public function getNationalPark()
    {
        return $this->national_park;
    }

    /**
     * Set sights
     *
     * @param \AppBundle\Entity\Sights $sights
     *
     * @return Images
     */
    public function setSights(\AppBundle\Entity\Sights $sights = null)
    {
        $this->sights = $sights;

        return $this;
    }

    /**
     * Get sights
     *
     * @return \AppBundle\Entity\Sights
     */
    public function getSights()
    {
        return $this->sights;
    }
    /**
     * @var \AppBundle\Entity\Drinking
     */
    private $drinking;

    /**
     * @var \AppBundle\Entity\Eating
     */
    private $eating;

    /**
     * @var \AppBundle\Entity\Sleeping
     */
    private $sleeping;


    /**
     * Set drinking
     *
     * @param \AppBundle\Entity\Drinking $drinking
     *
     * @return Images
     */
    public function setDrinking(\AppBundle\Entity\Drinking $drinking = null)
    {
        $this->drinking = $drinking;

        return $this;
    }

    /**
     * Get drinking
     *
     * @return \AppBundle\Entity\Drinking
     */
    public function getDrinking()
    {
        return $this->drinking;
    }

    /**
     * Set eating
     *
     * @param \AppBundle\Entity\Eating $eating
     *
     * @return Images
     */
    public function setEating(\AppBundle\Entity\Eating $eating = null)
    {
        $this->eating = $eating;

        return $this;
    }

    /**
     * Get eating
     *
     * @return \AppBundle\Entity\Eating
     */
    public function getEating()
    {
        return $this->eating;
    }

    /**
     * Set sleeping
     *
     * @param \AppBundle\Entity\Sleeping $sleeping
     *
     * @return Images
     */
    public function setSleeping(\AppBundle\Entity\Sleeping $sleeping = null)
    {
        $this->sleeping = $sleeping;

        return $this;
    }

    /**
     * Get sleeping
     *
     * @return \AppBundle\Entity\Sleeping
     */
    public function getSleeping()
    {
        return $this->sleeping;
    }
}
