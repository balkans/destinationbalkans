<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * State
 */
class State
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbStateName;

    /**
     * @var string
     */
    private $dbStateInfo;

    /**
     * @var string
     */
    private $dbStateDescription;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbStateName
     *
     * @param string $dbStateName
     * @return State
     */
    public function setDbStateName($dbStateName)
    {
        $this->dbStateName = $dbStateName;

        return $this;
    }

    /**
     * Get dbStateName
     *
     * @return string
     */
    public function getDbStateName()
    {
        return $this->dbStateName;
    }

    /**
     * Set dbStateInfo
     *
     * @param string $dbStateInfo
     * @return State
     */
    public function setDbStateInfo($dbStateInfo)
    {
        $this->dbStateInfo = $dbStateInfo;

        return $this;
    }

    /**
     * Get dbStateInfo
     *
     * @return string
     */
    public function getDbStateInfo()
    {
        return $this->dbStateInfo;
    }

    /**
     * Set dbStateDescription
     *
     * @param string $dbStateDescription
     * @return State
     */
    public function setDbStateDescription($dbStateDescription)
    {
        $this->dbStateDescription = $dbStateDescription;

        return $this;
    }

    /**
     * Get dbStateDescription
     *
     * @return string
     */
    public function getDbStateDescription()
    {
        return $this->dbStateDescription;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $city;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->city = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add city
     *
     * @param \AppBundle\Entity\City $city
     * @return State
     */
    public function addCity(\AppBundle\Entity\City $city)
    {
        $this->city[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param \AppBundle\Entity\City $city
     */
    public function removeCity(\AppBundle\Entity\City $city)
    {
        $this->city->removeElement($city);
    }

    /**
     * Get city
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $national_park;


    /**
     * Add national_park
     *
     * @param \AppBundle\Entity\NationalPark $nationalPark
     * @return State
     */
    public function addNationalPark(\AppBundle\Entity\NationalPark $nationalPark)
    {
        $this->national_park[] = $nationalPark;

        return $this;
    }

    /**
     * Remove national_park
     *
     * @param \AppBundle\Entity\NationalPark $nationalPark
     */
    public function removeNationalPark(\AppBundle\Entity\NationalPark $nationalPark)
    {
        $this->national_park->removeElement($nationalPark);
    }

    /**
     * Get national_park
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNationalPark()
    {
        return $this->national_park;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $getaways;


    /**
     * Add getaways
     *
     * @param \AppBundle\Entity\Getaways $getaways
     * @return State
     */
    public function addGetaway(\AppBundle\Entity\Getaways $getaways)
    {
        $this->getaways[] = $getaways;

        return $this;
    }

    /**
     * Remove getaways
     *
     * @param \AppBundle\Entity\Getaways $getaways
     */
    public function removeGetaway(\AppBundle\Entity\Getaways $getaways)
    {
        $this->getaways->removeElement($getaways);
    }

    /**
     * Get getaways
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGetaways()
    {
        return $this->getaways;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;


    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return State
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
