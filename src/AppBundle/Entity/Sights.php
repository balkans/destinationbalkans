<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sights
 */
class Sights
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbSightsName;

    /**
     * @var string
     */
    private $dbSightsInfo;

    /**
     * @var string
     */
    private $dbSightsDescription;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbSightsName
     *
     * @param string $dbSightsName
     * @return Sights
     */
    public function setDbSightsName($dbSightsName)
    {
        $this->dbSightsName = $dbSightsName;

        return $this;
    }

    /**
     * Get dbSightsName
     *
     * @return string
     */
    public function getDbSightsName()
    {
        return $this->dbSightsName;
    }

    /**
     * Set dbSightsInfo
     *
     * @param string $dbSightsInfo
     * @return Sights
     */
    public function setDbSightsInfo($dbSightsInfo)
    {
        $this->dbSightsInfo = $dbSightsInfo;

        return $this;
    }

    /**
     * Get dbSightsInfo
     *
     * @return string
     */
    public function getDbSightsInfo()
    {
        return $this->dbSightsInfo;
    }

    /**
     * Set dbSightsDescription
     *
     * @param string $dbSightsDescription
     * @return Sights
     */
    public function setDbSightsDescription($dbSightsDescription)
    {
        $this->dbSightsDescription = $dbSightsDescription;

        return $this;
    }

    /**
     * Get dbSightsDescription
     *
     * @return string
     */
    public function getDbSightsDescription()
    {
        return $this->dbSightsDescription;
    }
    /**
     * @var \AppBundle\Entity\City
     */
    private $city;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     * @return Sights
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Sights
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
    /**
     * @var \AppBundle\Entity\NationalPark
     */
    private $national_parks;


    /**
     * Set nationalParks
     *
     * @param \AppBundle\Entity\NationalPark $nationalParks
     *
     * @return Sights
     */
    public function setNationalParks(\AppBundle\Entity\NationalPark $nationalParks = null)
    {
        $this->national_parks = $nationalParks;

        return $this;
    }

    /**
     * Get nationalParks
     *
     * @return \AppBundle\Entity\NationalPark
     */
    public function getNationalParks()
    {
        return $this->national_parks;
    }
    /**
     * @var \AppBundle\Entity\NationalPark
     */
    private $national_park;


    /**
     * Set nationalPark
     *
     * @param \AppBundle\Entity\NationalPark $nationalPark
     *
     * @return Sights
     */
    public function setNationalPark(\AppBundle\Entity\NationalPark $nationalPark = null)
    {
        $this->national_park = $nationalPark;

        return $this;
    }

    /**
     * Get nationalPark
     *
     * @return \AppBundle\Entity\NationalPark
     */
    public function getNationalPark()
    {
        return $this->national_park;
    }
}
