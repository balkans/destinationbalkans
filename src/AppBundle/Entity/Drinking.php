<?php

namespace AppBundle\Entity;

/**
 * Drinking
 */
class Drinking
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbDrinkingName;

    /**
     * @var string
     */
    private $dbDrinkingInfo;


    /**
     * @var string
     */
    private $dbDrinkingLocation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbDrinkingName
     *
     * @param string $dbDrinkingName
     *
     * @return Drinking
     */
    public function setDbDrinkingName($dbDrinkingName)
    {
        $this->dbDrinkingName = $dbDrinkingName;

        return $this;
    }

    /**
     * Get dbDrinkingName
     *
     * @return string
     */
    public function getDbDrinkingName()
    {
        return $this->dbDrinkingName;
    }

    /**
     * Set dbDrinkingInfo
     *
     * @param string $dbDrinkingInfo
     *
     * @return Drinking
     */
    public function setDbDrinkingInfo($dbDrinkingInfo)
    {
        $this->dbDrinkingInfo = $dbDrinkingInfo;

        return $this;
    }

    /**
     * Get dbDrinkingInfo
     *
     * @return string
     */
    public function getDbDrinkingInfo()
    {
        return $this->dbDrinkingInfo;
    }
    

    /**
     * Set dbDrinkingLocation
     *
     * @param string $dbDrinkingLocation
     *
     * @return Drinking
     */
    public function setDbDrinkingLocation($dbDrinkingLocation)
    {
        $this->dbDrinkingLocation = $dbDrinkingLocation;

        return $this;
    }

    /**
     * Get dbDrinkingLocation
     *
     * @return string
     */
    public function getDbDrinkingLocation()
    {
        return $this->dbDrinkingLocation;
    }
    /**
     * @var \AppBundle\Entity\City
     */
    private $city;


    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Drinking
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @var \AppBundle\Entity\NationalPark
     */
    private $national_park;


    /**
     * Set nationalPark
     *
     * @param \AppBundle\Entity\NationalPark $nationalPark
     *
     * @return Drinking
     */
    public function setNationalPark(\AppBundle\Entity\NationalPark $nationalPark = null)
    {
        $this->national_park = $nationalPark;

        return $this;
    }

    /**
     * Get nationalPark
     *
     * @return \AppBundle\Entity\NationalPark
     */
    public function getNationalPark()
    {
        return $this->national_park;
    }
    /**
     * @var \AppBundle\Entity\Getaways
     */
    private $getaways;


    /**
     * Set getaways
     *
     * @param \AppBundle\Entity\Getaways $getaways
     *
     * @return Drinking
     */
    public function setGetaways(\AppBundle\Entity\Getaways $getaways = null)
    {
        $this->getaways = $getaways;

        return $this;
    }

    /**
     * Get getaways
     *
     * @return \AppBundle\Entity\Getaways
     */
    public function getGetaways()
    {
        return $this->getaways;
    }
    /**
     * @var string
     */
    private $dbSleepingStatus;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Set dbSleepingStatus
     *
     * @param string $dbSleepingStatus
     *
     * @return Drinking
     */
    public function setDbSleepingStatus($dbSleepingStatus)
    {
        $this->dbSleepingStatus = $dbSleepingStatus;

        return $this;
    }

    /**
     * Get dbSleepingStatus
     *
     * @return string
     */
    public function getDbSleepingStatus()
    {
        return $this->dbSleepingStatus;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Drinking
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var string
     */
    private $dbDrinkingStatus;


    /**
     * Set dbDrinkingStatus
     *
     * @param string $dbDrinkingStatus
     *
     * @return Drinking
     */
    public function setDbDrinkingStatus($dbDrinkingStatus)
    {
        $this->dbDrinkingStatus = $dbDrinkingStatus;

        return $this;
    }

    /**
     * Get dbDrinkingStatus
     *
     * @return string
     */
    public function getDbDrinkingStatus()
    {
        return $this->dbDrinkingStatus;
    }
    /**
     * @var string
     */
    private $dbDrinkingWeb;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dbDrinkingWeb
     *
     * @param string $dbDrinkingWeb
     *
     * @return Drinking
     */
    public function setDbDrinkingWeb($dbDrinkingWeb)
    {
        $this->dbDrinkingWeb = $dbDrinkingWeb;

        return $this;
    }

    /**
     * Get dbDrinkingWeb
     *
     * @return string
     */
    public function getDbDrinkingWeb()
    {
        return $this->dbDrinkingWeb;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Drinking
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
