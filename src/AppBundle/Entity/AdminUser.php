<?php

namespace AppBundle\Entity;

/**
 * AdminUser
 */
class AdminUser
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbUserName;

    /**
     * @var string
     */
    private $dbUserPassword;

    /**
     * @var string
     */
    private $dbUserStatus;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbUserName
     *
     * @param string $dbUserName
     *
     * @return AdminUser
     */
    public function setDbUserName($dbUserName)
    {
        $this->dbUserName = $dbUserName;

        return $this;
    }

    /**
     * Get dbUserName
     *
     * @return string
     */
    public function getDbUserName()
    {
        return $this->dbUserName;
    }

    /**
     * Set dbUserPassword
     *
     * @param string $dbUserPassword
     *
     * @return AdminUser
     */
    public function setDbUserPassword($dbUserPassword)
    {
        $this->dbUserPassword = $dbUserPassword;

        return $this;
    }

    /**
     * Get dbUserPassword
     *
     * @return string
     */
    public function getDbUserPassword()
    {
        return $this->dbUserPassword;
    }

    /**
     * Set dbUserStatus
     *
     * @param string $dbUserStatus
     *
     * @return AdminUser
     */
    public function setDbUserStatus($dbUserStatus)
    {
        $this->dbUserStatus = $dbUserStatus;

        return $this;
    }

    /**
     * Get dbUserStatus
     *
     * @return string
     */
    public function getDbUserStatus()
    {
        return $this->dbUserStatus;
    }
}
