<?php

namespace AppBundle\Entity;

/**
 * Eating
 */
class Eating
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbEatingName;

    /**
     * @var string
     */
    private $dbEatingInfo;


    /**
     * @var string
     */
    private $dbEatingLocation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbEatingName
     *
     * @param string $dbEatingName
     *
     * @return Eating
     */
    public function setDbEatingName($dbEatingName)
    {
        $this->dbEatingName = $dbEatingName;

        return $this;
    }

    /**
     * Get dbEatingName
     *
     * @return string
     */
    public function getDbEatingName()
    {
        return $this->dbEatingName;
    }

    /**
     * Set dbEatingInfo
     *
     * @param string $dbEatingInfo
     *
     * @return Eating
     */
    public function setDbEatingInfo($dbEatingInfo)
    {
        $this->dbEatingInfo = $dbEatingInfo;

        return $this;
    }

    /**
     * Get dbEatingInfo
     *
     * @return string
     */
    public function getDbEatingInfo()
    {
        return $this->dbEatingInfo;
    }
    

    /**
     * Set dbEatingLocation
     *
     * @param string $dbEatingLocation
     *
     * @return Eating
     */
    public function setDbEatingLocation($dbEatingLocation)
    {
        $this->dbEatingLocation = $dbEatingLocation;

        return $this;
    }

    /**
     * Get dbEatingLocation
     *
     * @return string
     */
    public function getDbEatingLocation()
    {
        return $this->dbEatingLocation;
    }
    /**
     * @var \AppBundle\Entity\City
     */
    private $city;


    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Eating
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * @var \AppBundle\Entity\Getaways
     */
    private $getaways;

    /**
     * @var \AppBundle\Entity\NationalPark
     */
    private $national_park;


    /**
     * Set getaways
     *
     * @param \AppBundle\Entity\Getaways $getaways
     *
     * @return Eating
     */
    public function setGetaways(\AppBundle\Entity\Getaways $getaways = null)
    {
        $this->getaways = $getaways;

        return $this;
    }

    /**
     * Get getaways
     *
     * @return \AppBundle\Entity\Getaways
     */
    public function getGetaways()
    {
        return $this->getaways;
    }

    /**
     * Set nationalPark
     *
     * @param \AppBundle\Entity\NationalPark $nationalPark
     *
     * @return Eating
     */
    public function setNationalPark(\AppBundle\Entity\NationalPark $nationalPark = null)
    {
        $this->national_park = $nationalPark;

        return $this;
    }

    /**
     * Get nationalPark
     *
     * @return \AppBundle\Entity\NationalPark
     */
    public function getNationalPark()
    {
        return $this->national_park;
    }


    /**
     * @var string
     */
    private $dbEatingStatus;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Set dbEatingStatus
     *
     * @param string $dbEatingStatus
     *
     * @return Eating
     */
    public function setDbEatingStatus($dbEatingStatus)
    {
        $this->dbEatingStatus = $dbEatingStatus;

        return $this;
    }

    /**
     * Get dbEatingStatus
     *
     * @return string
     */
    public function getDbEatingStatus()
    {
        return $this->dbEatingStatus;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Eating
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var string
     */
    private $dbEatingWeb;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dbEatingWeb
     *
     * @param string $dbEatingWeb
     *
     * @return Eating
     */
    public function setDbEatingWeb($dbEatingWeb)
    {
        $this->dbEatingWeb = $dbEatingWeb;

        return $this;
    }

    /**
     * Get dbEatingWeb
     *
     * @return string
     */
    public function getDbEatingWeb()
    {
        return $this->dbEatingWeb;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Eating
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
