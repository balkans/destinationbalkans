<?php

namespace AppBundle\Entity;

/**
 * Sleeping
 */
class Sleeping
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbSleepingName;

    /**
     * @var string
     */
    private $dbSleepingInfo;


    /**
     * @var string
     */
    private $dbSleepingLocation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbSleepingName
     *
     * @param string $dbSleepingName
     *
     * @return Sleeping
     */
    public function setDbSleepingName($dbSleepingName)
    {
        $this->dbSleepingName = $dbSleepingName;

        return $this;
    }

    /**
     * Get dbSleepingName
     *
     * @return string
     */
    public function getDbSleepingName()
    {
        return $this->dbSleepingName;
    }

    /**
     * Set dbSleepingInfo
     *
     * @param string $dbSleepingInfo
     *
     * @return Sleeping
     */
    public function setDbSleepingInfo($dbSleepingInfo)
    {
        $this->dbSleepingInfo = $dbSleepingInfo;

        return $this;
    }

    /**
     * Get dbSleepingInfo
     *
     * @return string
     */
    public function getDbSleepingInfo()
    {
        return $this->dbSleepingInfo;
    }
    
    /**
     * Set dbSleepingLocation
     *
     * @param string $dbSleepingLocation
     *
     * @return Sleeping
     */
    public function setDbSleepingLocation($dbSleepingLocation)
    {
        $this->dbSleepingLocation = $dbSleepingLocation;

        return $this;
    }

    /**
     * Get dbSleepingLocation
     *
     * @return string
     */
    public function getDbSleepingLocation()
    {
        return $this->dbSleepingLocation;
    }
    /**
     * @var \AppBundle\Entity\City
     */
    private $city;


    /**
     * Set city
     *
     * @param \AppBundle\Entity\City $city
     *
     * @return Sleeping
     */
    public function setCity(\AppBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \AppBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @var \AppBundle\Entity\NationalPark
     */
    private $national_park;


    /**
     * Set nationalPark
     *
     * @param \AppBundle\Entity\NationalPark $nationalPark
     *
     * @return Sleeping
     */
    public function setNationalPark(\AppBundle\Entity\NationalPark $nationalPark = null)
    {
        $this->national_park = $nationalPark;

        return $this;
    }

    /**
     * Get nationalPark
     *
     * @return \AppBundle\Entity\NationalPark
     */
    public function getNationalPark()
    {
        return $this->national_park;
    }
    /**
     * @var \AppBundle\Entity\Getaways
     */
    private $getaways;


    /**
     * Set getaways
     *
     * @param \AppBundle\Entity\Getaways $getaways
     *
     * @return Sleeping
     */
    public function setGetaways(\AppBundle\Entity\Getaways $getaways = null)
    {
        $this->getaways = $getaways;

        return $this;
    }

    /**
     * Get getaways
     *
     * @return \AppBundle\Entity\Getaways
     */
    public function getGetaways()
    {
        return $this->getaways;
    }
    /**
     * @var string
     */
    private $dbSleepingStatus;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Set dbSleepingStatus
     *
     * @param string $dbSleepingStatus
     *
     * @return Sleeping
     */
    public function setDbSleepingStatus($dbSleepingStatus)
    {
        $this->dbSleepingStatus = $dbSleepingStatus;

        return $this;
    }

    /**
     * Get dbSleepingStatus
     *
     * @return string
     */
    public function getDbSleepingStatus()
    {
        return $this->dbSleepingStatus;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Sleeping
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var string
     */
    private $dbSleepingWeb;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dbSleepingWeb
     *
     * @param string $dbSleepingWeb
     *
     * @return Sleeping
     */
    public function setDbSleepingWeb($dbSleepingWeb)
    {
        $this->dbSleepingWeb = $dbSleepingWeb;

        return $this;
    }

    /**
     * Get dbSleepingWeb
     *
     * @return string
     */
    public function getDbSleepingWeb()
    {
        return $this->dbSleepingWeb;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Sleeping
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
