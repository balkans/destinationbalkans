<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NationalPark
 */
class NationalPark
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbParkName;

    /**
     * @var string
     */
    private $dbParkInfo;

    /**
     * @var string
     */
    private $dbParkDescription;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbParkName
     *
     * @param string $dbParkName
     * @return NationalPark
     */
    public function setDbParkName($dbParkName)
    {
        $this->dbParkName = $dbParkName;

        return $this;
    }

    /**
     * Get dbParkName
     *
     * @return string
     */
    public function getDbParkName()
    {
        return $this->dbParkName;
    }

    /**
     * Set dbParkInfo
     *
     * @param string $dbParkInfo
     * @return NationalPark
     */
    public function setDbParkInfo($dbParkInfo)
    {
        $this->dbParkInfo = $dbParkInfo;

        return $this;
    }

    /**
     * Get dbParkInfo
     *
     * @return string
     */
    public function getDbParkInfo()
    {
        return $this->dbParkInfo;
    }

    /**
     * Set dbParkDescription
     *
     * @param string $dbParkDescription
     * @return NationalPark
     */
    public function setDbParkDescription($dbParkDescription)
    {
        $this->dbParkDescription = $dbParkDescription;

        return $this;
    }

    /**
     * Get dbParkDescription
     *
     * @return string
     */
    public function getDbParkDescription()
    {
        return $this->dbParkDescription;
    }

    /**
     * @var \AppBundle\Entity\State
     */
    private $state;


    /**
     * Set state
     *
     * @param \AppBundle\Entity\State $state
     * @return NationalPark
     */
    public function setState(\AppBundle\Entity\State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \AppBundle\Entity\State
     */
    public function getState()
    {
        return $this->state;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return NationalPark
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $drinking;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eating;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sleeping;


    /**
     * Add drinking
     *
     * @param \AppBundle\Entity\Drinking $drinking
     *
     * @return NationalPark
     */
    public function addDrinking(\AppBundle\Entity\Drinking $drinking)
    {
        $this->drinking[] = $drinking;

        return $this;
    }

    /**
     * Remove drinking
     *
     * @param \AppBundle\Entity\Drinking $drinking
     */
    public function removeDrinking(\AppBundle\Entity\Drinking $drinking)
    {
        $this->drinking->removeElement($drinking);
    }

    /**
     * Get drinking
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDrinking()
    {
        return $this->drinking;
    }

    /**
     * Add eating
     *
     * @param \AppBundle\Entity\Eating $eating
     *
     * @return NationalPark
     */
    public function addEating(\AppBundle\Entity\Eating $eating)
    {
        $this->eating[] = $eating;

        return $this;
    }

    /**
     * Remove eating
     *
     * @param \AppBundle\Entity\Eating $eating
     */
    public function removeEating(\AppBundle\Entity\Eating $eating)
    {
        $this->eating->removeElement($eating);
    }

    /**
     * Get eating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEating()
    {
        return $this->eating;
    }

    /**
     * Add sleeping
     *
     * @param \AppBundle\Entity\Sleeping $sleeping
     *
     * @return NationalPark
     */
    public function addSleeping(\AppBundle\Entity\Sleeping $sleeping)
    {
        $this->sleeping[] = $sleeping;

        return $this;
    }

    /**
     * Remove sleeping
     *
     * @param \AppBundle\Entity\Sleeping $sleeping
     */
    public function removeSleeping(\AppBundle\Entity\Sleeping $sleeping)
    {
        $this->sleeping->removeElement($sleeping);
    }

    /**
     * Get sleeping
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSleeping()
    {
        return $this->sleeping;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sights;


    /**
     * Add sight
     *
     * @param \AppBundle\Entity\Sights $sight
     *
     * @return NationalPark
     */
    public function addSight(\AppBundle\Entity\Sights $sight)
    {
        $this->sights[] = $sight;

        return $this;
    }

    /**
     * Remove sight
     *
     * @param \AppBundle\Entity\Sights $sight
     */
    public function removeSight(\AppBundle\Entity\Sights $sight)
    {
        $this->sights->removeElement($sight);
    }

    /**
     * Get sights
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSights()
    {
        return $this->sights;
    }
}
