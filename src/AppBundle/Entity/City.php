<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 */
class City
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dbCityName;

    /**
     * @var string
     */
    private $dbCityInfo;

    /**
     * @var string
     */
    private $dbCityDescription;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dbCityName
     *
     * @param string $dbCityName
     * @return City
     */
    public function setDbCityName($dbCityName)
    {
        $this->dbCityName = $dbCityName;

        return $this;
    }

    /**
     * Get dbCityName
     *
     * @return string
     */
    public function getDbCityName()
    {
        return $this->dbCityName;
    }

    /**
     * Set dbCityInfo
     *
     * @param string $dbCityInfo
     * @return City
     */
    public function setDbCityInfo($dbCityInfo)
    {
        $this->dbCityInfo = $dbCityInfo;

        return $this;
    }

    /**
     * Get dbCityInfo
     *
     * @return string
     */
    public function getDbCityInfo()
    {
        return $this->dbCityInfo;
    }

    /**
     * Set dbCityDescription
     *
     * @param string $dbCityDescription
     * @return City
     */
    public function setDbCityDescription($dbCityDescription)
    {
        $this->dbCityDescription = $dbCityDescription;

        return $this;
    }

    /**
     * Get dbCityDescription
     *
     * @return string
     */
    public function getDbCityDescription()
    {
        return $this->dbCityDescription;
    }
    /**
     * @var \AppBundle\Entity\State
     */
    private $state;


    /**
     * Set state
     *
     * @param \AppBundle\Entity\State $state
     * @return City
     */
    public function setState(\AppBundle\Entity\State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \AppBundle\Entity\State
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sights;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sights = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sights
     *
     * @param \AppBundle\Entity\Sights $sights
     * @return City
     */
    public function addSight(\AppBundle\Entity\Sights $sights)
    {
        $this->sights[] = $sights;

        return $this;
    }

    /**
     * Remove sights
     *
     * @param \AppBundle\Entity\Sights $sights
     */
    public function removeSight(\AppBundle\Entity\Sights $sights)
    {
        $this->sights->removeElement($sights);
    }

    /**
     * Get sights
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSights()
    {
        return $this->sights;
    }


    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return City
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $drinking;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eating;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sleeping;


    /**
     * Add drinking
     *
     * @param \AppBundle\Entity\Drinking $drinking
     *
     * @return City
     */
    public function addDrinking(\AppBundle\Entity\Drinking $drinking)
    {
        $this->drinking[] = $drinking;

        return $this;
    }

    /**
     * Remove drinking
     *
     * @param \AppBundle\Entity\Drinking $drinking
     */
    public function removeDrinking(\AppBundle\Entity\Drinking $drinking)
    {
        $this->drinking->removeElement($drinking);
    }

    /**
     * Get drinking
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDrinking()
    {
        return $this->drinking;
    }

    /**
     * Add eating
     *
     * @param \AppBundle\Entity\Eating $eating
     *
     * @return City
     */
    public function addEating(\AppBundle\Entity\Eating $eating)
    {
        $this->eating[] = $eating;

        return $this;
    }

    /**
     * Remove eating
     *
     * @param \AppBundle\Entity\Eating $eating
     */
    public function removeEating(\AppBundle\Entity\Eating $eating)
    {
        $this->eating->removeElement($eating);
    }

    /**
     * Get eating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEating()
    {
        return $this->eating;
    }

    /**
     * Add sleeping
     *
     * @param \AppBundle\Entity\Sleeping $sleeping
     *
     * @return City
     */
    public function addSleeping(\AppBundle\Entity\Sleeping $sleeping)
    {
        $this->sleeping[] = $sleeping;

        return $this;
    }

    /**
     * Remove sleeping
     *
     * @param \AppBundle\Entity\Sleeping $sleeping
     */
    public function removeSleeping(\AppBundle\Entity\Sleeping $sleeping)
    {
        $this->sleeping->removeElement($sleeping);
    }

    /**
     * Get sleeping
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSleeping()
    {
        return $this->sleeping;
    }
}
