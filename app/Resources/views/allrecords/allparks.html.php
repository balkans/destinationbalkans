<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <title></title>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/main.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" type="image/x-icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
    </head>
		<body>
    <!-- Wrapper -->
      <div id="wrapper">

        <!-- Header -->
          <header id="header" class="alt">
            <a href="/" class="logo"><strong>Destination Balkans</strong></a>
              <a href="http://www.georgemilojevic.net/" class="logo"><span>georgemilojevic.net</span></a>
            <nav>
              <a href="#menu">Menu</a>
            </nav>
          </header>

				<!-- Menu -->
					<nav id="menu">
						<ul class="links">
							<li><a href="/">Home</a></li>
							<li><a href="../allcities/">Cities</a></li>
							<li><a href="../allnationalparks/">National Parks</a></li>
							<li><a href="../allgetaways">Getaways</a></li>
						</ul>
						<ul class="actions vertical">
							<li><a href="#" class="button special fit">Get Started</a></li>
							<li><a href="#" class="button fit">Log In</a></li>
						</ul>
					</nav>

				<!-- Banner -->
        <section id="banner" class="style2">
          <div class="inner">
            <span class="image">
              <img src="<?php echo $view['assets']->getUrl('images/products/'.$selstate['0']['dbImageFile'])?>" alt="" />
            </span>
            <header class="major">
              <h1>Destination:<?=$selstate['0']['dbStateName']?></h1>
            </header>
            <div class="content">
              <p><?=$selstate['0']['dbStateInfo']?></p>
            </div>
          </div>
        </section>

      <!-- Main -->
        <div id="main">

          <!-- One -->
            <section id="one">
              <div class="inner">
                <header class="major">
                  <h2>More Info:</h2>
                </header>
                <p><?=$selstate['0']['dbStateDescription']?></p>
              </div>
            </section>

						<!-- Two -->
							<section id="two" class="spotlights">
                <?php foreach ($selstate as $np=>$v): ?>
								<section>
									<a href="../nationalpark/<?=$v['dbParkName']?>" class="image">
										<img src="<?php echo $view['assets']->getUrl('images/products/'.$v['dbImageFile']) ?>" alt="" data-position="center center"/>
									</a>
									<div class="content">
										<div class="inner">
											<header class="major">
												<h3><?=$v['dbParkName']?></h3>
											</header>
											<p><?=$v['dbParkInfo']?></p>
											<ul class="actions">
												<li><a href="../nationalpark/<?=$v['dbParkName']?>" class="button">Read More</a></li>
											</ul>
										</div>
									</div>
								</section>
                <?php endforeach ?>
							</section>


				<!-- Footer -->
					<footer id="footer">
						<div class="inner">
							<ul class="icons">
								<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
								<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
							</ul>
							<ul class="copyright">
								<li>&copy; Untitled</li>
							</ul>
						</div>
					</footer>

			</div>
			<!-- Scripts -->
			<script src="<?php echo $view['assets']->getUrl('js/jquery.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrolly.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrollex.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/skel.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/util.js') ?>"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo $view['assets']->getUrl('js/main.js') ?>"></script>

		</body>
</html>
