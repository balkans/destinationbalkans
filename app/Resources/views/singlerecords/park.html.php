<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <title></title>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/main.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" type="image/x-icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
    </head>
		<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
            <a href="/" class="logo"><strong>Destination Balkans</strong></a>
              <a href="http://www.georgemilojevic.net/" class="logo"><span>georgemilojevic.net</span></a>
						<nav>
							<a href="#menu">Menu</a>
						</nav>
					</header>

					<!-- Menu -->
          <nav id="menu">
            <ul class="links">
              <li><a href="/">Home</a></li>
              <li><a href="../../allcities/">Cities</a></li>
              <li><a href="../../allgetaways/">Getaways</a></li>
              <li><a href="../../allnationalparks/">National Parks</a></li>
              <li><a href="/">State</a></li>
            </ul>
          </nav>

				<!-- Main -->
					<div id="main" class="alt">

						<!-- One -->
							<section id="one">
								<div class="inner">
									<header class="major">
										<h1><?=$national_parks->getDbParkName()?></h1>
									</header>
									<span class="image main"><img src="<?php echo $view['assets']->getUrl('images/products/'.$photo[0]->getDbImageFile())?>" alt="" /></span>
									<p><?=$national_parks->getDbParkInfo()?></p>
									<p><?=$national_parks->getDbParkDescription()?></p>

								</div>
							</section>

						<section id="two" class="spotlights">
							<?php foreach ($sight as $sights => $sg): ?>
								<section>
									<a href="" class="image">
										<img src="<?php echo $view['assets']->getUrl('images/products/'.$sg['dbImageFile'])?>" alt="" data-position="center center"/>
									</a>
									<div class="content">
										<div class="inner">
											<header class="major">
												<h3><?=$sg['dbSightsName']?></h3>
											</header>
											<p><?=$sg['dbSightsInfo']?></p>
											<p><?=$sg['dbSightsDescription']?></p>
											<ul class="actions">
												<li><a href="#" class="button">Read More</a></li>
											</ul>
										</div>
									</div>
								</section>
							<?php endforeach ?>
						</section>

					</div>

				<!-- Contact -->
					<section id="contact">
						<div class="inner">
							<section>
								<form method="post" action="#">
									<div class="field half first">
										<label for="name">Name</label>
										<input type="text" name="name" id="name" />
									</div>
									<div class="field half">
										<label for="email">Email</label>
										<input type="text" name="email" id="email" />
									</div>
									<div class="field">
										<label for="message">Message</label>
										<textarea name="message" id="message" rows="6"></textarea>
									</div>
									<ul class="actions">
										<li><input type="submit" value="Send Message" class="special" /></li>
										<li><input type="reset" value="Clear" /></li>
									</ul>
								</form>
							</section>
							<section class="split">
								<section>
									<div class="contact-method">
										<span class="icon alt fa-envelope"></span>
										<h3>Email</h3>
										<a href="#">information@untitled.tld</a>
									</div>
								</section>
								<section>
									<div class="contact-method">
										<span class="icon alt fa-phone"></span>
										<h3>Phone</h3>
										<span>(000) 000-0000 x12387</span>
									</div>
								</section>
								<section>
									<div class="contact-method">
										<span class="icon alt fa-home"></span>
										<h3>Address</h3>
										<span>1234 Somewhere Road #5432<br />
										Nashville, TN 00000<br />
										United States of America</span>
									</div>
								</section>
							</section>
						</div>
					</section>

				<!-- Footer -->
					<footer id="footer">
						<div class="inner">
							<ul class="icons">
								<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
								<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
							</ul>
							<ul class="copyright">
								<li>&copy; Untitled</li>
							</ul>
						</div>
					</footer>

			</div>
			<!-- Scripts -->
			<script src="<?php echo $view['assets']->getUrl('js/jquery.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrolly.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrollex.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/skel.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/util.js') ?>"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo $view['assets']->getUrl('js/main.js') ?>"></script>

</body>
</html>
