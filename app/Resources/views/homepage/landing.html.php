<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <title></title>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/main.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" type="image/x-icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
    </head>
		<body>
    <!-- Wrapper -->
      <div id="wrapper">

        <!-- Header -->
          <header id="header" class="alt">
            <a href="/" class="logo"><strong>Destination Balkans</strong></a>
              <a href="http://www.georgemilojevic.net/" class="logo"><span>georgemilojevic.net</span></a>
            <nav>
              <a href="#menu">Menu</a>
            </nav>
          </header>

				<!-- Menu -->
					<nav id="menu">
						<ul class="links">
							<li><a href="/">Home</a></li>
							<li><a href="allcities/">Cities</a></li>
							<li><a href="allnationalparks/">National Parks</a></li>
							<li><a href="allgetaways">Getaways</a></li>
						</ul>
						<ul class="actions vertical">
							<li><a href="#" class="button special fit">Get Started</a></li>
							<li><a href="#" class="button fit">Log In</a></li>
						</ul>
					</nav>

				<!-- Banner -->
				<!-- Note: The "styleN" class below should match that of the header element. -->
					<section id="banner" class="style2">
						<div class="inner">
							<span class="image">
								<img src="<?php echo $view['assets']->getUrl('images/products/'.$randomstatephoto['dbImageFile'])?>" alt="" />
							</span>
							<header class="major">

								<h1>Destination: <?=$randomstatephoto['dbStateName']?></h1>
							</header>
							<div class="content">
								<p><?=$randomstatephoto['dbStateInfo']?></p>
							</div>
						</div>
					</section>

				<!-- Main -->
					<div id="main">

						<!-- One -->
							<section id="one">
								<div class="inner">
									<header class="major">
										<h2>More Info: <?=$randomstatephoto['dbStateName']?></h2>
									</header>
									<p><?=$randomstatephoto['dbStateDescription']?></p>
								</div>
							</section>

						<!-- Two -->
							<section id="two" class="spotlights">
								<section>
									<a href="city/<?=$randomcity['dbCityName']?>" class="image">
										<img src="<?php echo $view['assets']->getUrl('images/products/'.$randomcity['dbImageFile'])?>" alt="" data-position="center center"/>
									</a>
									<div class="content">
										<div class="inner">
											<header class="major">
												<h3><?=$randomcity['dbCityName']?></h3>
											</header>
											<p><?=$randomcity['dbCityInfo']?></p>
											<ul class="actions">
												<li><a href="allcities/" class="button">See All Cities</a></li>
											</ul>
										</div>
									</div>
								</section>
								<section>
									<a href="nationalpark/<?=$randomnp['dbParkName']?>" class="image">
											<img src="<?php echo $view['assets']->getUrl('images/products/'.$randomnp['dbImageFile'])?>" alt="" data-position="top center"/>
									</a>
									<div class="content">
										<div class="inner">
											<header class="major">
												<h3><?=$randomnp['dbParkName']?></h3>
											</header>
											<p><?=$randomnp['dbParkInfo']?></p>
											<ul class="actions">
												<li><a href="allnationalparks/" class="button">See All National Parks</a></li>
											</ul>
										</div>
									</div>
								</section>
								<section>
									<a href="getaway/<?=$randomgetaway['dbGetawaysName']?>" class="image">
											<img src="<?php echo $view['assets']->getUrl('images/products/'.$randomgetaway['dbImageFile']) ?>" alt="" data-position="25% 25%"/>
									</a>
									<div class="content">
										<div class="inner">
											<header class="major">
												<h3><?=$randomgetaway['dbGetawaysName']?></h3>
											</header>
											<p><?=$randomgetaway['dbGetawaysInfo']?></p>
											<ul class="actions">
												<li><a href="allgetaways/" class="button">See All Getaways</a></li>
											</ul>
										</div>
									</div>
								</section>
							</section>

						<!-- Three -->
							<section id="three">
								<div class="inner">
									<header class="major">
										<h2>Massa libero</h2>
									</header>
									<p>Nullam et orci eu lorem consequat tincidunt vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus pharetra. Pellentesque condimentum sem. In efficitur ligula tate urna. Maecenas laoreet massa vel lacinia pellentesque lorem ipsum dolor. Nullam et orci eu lorem consequat tincidunt. Vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus amet pharetra et feugiat tempus.</p>
									<ul class="actions">
										<li><a href="generic.html" class="button next">Get Started</a></li>
									</ul>
								</div>
							</section>

					</div>

				<!-- Contact -->
					<section id="contact">
						<div class="inner">
							<section>
								<form method="post" action="#">
									<div class="field half first">
										<label for="name">Name</label>
										<input type="text" name="name" id="name" />
									</div>
									<div class="field half">
										<label for="email">Email</label>
										<input type="text" name="email" id="email" />
									</div>
									<div class="field">
										<label for="message">Message</label>
										<textarea name="message" id="message" rows="6"></textarea>
									</div>
									<ul class="actions">
										<li><input type="submit" value="Send Message" class="special" /></li>
										<li><input type="reset" value="Clear" /></li>
									</ul>
								</form>
							</section>
							<section class="split">
								<section>
									<div class="contact-method">
										<span class="icon alt fa-envelope"></span>
										<h3>Email</h3>
										<a href="#">information@untitled.tld</a>
									</div>
								</section>
								<section>
									<div class="contact-method">
										<span class="icon alt fa-phone"></span>
										<h3>Phone</h3>
										<span>(000) 000-0000 x12387</span>
									</div>
								</section>
								<section>
									<div class="contact-method">
										<span class="icon alt fa-home"></span>
										<h3>Address</h3>
										<span>1234 Somewhere Road #5432<br />
										Nashville, TN 00000<br />
										United States of America</span>
									</div>
								</section>
							</section>
						</div>
					</section>

				<!-- Footer -->
					<footer id="footer">
						<div class="inner">
							<ul class="icons">
								<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
								<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
							</ul>
							<ul class="copyright">
								<li>&copy; Untitled</li>
							</ul>
						</div>
					</footer>

			</div>
			<!-- Scripts -->
			<script src="<?php echo $view['assets']->getUrl('js/jquery.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrolly.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrollex.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/skel.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/util.js') ?>"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo $view['assets']->getUrl('js/main.js') ?>"></script>

		</body>
</html>
