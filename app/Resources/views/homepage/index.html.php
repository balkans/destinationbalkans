<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <title></title>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/main.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" type="image/x-icon" href="<?php echo $view['assets']->getUrl('favicon.ico') ?>" />
    </head>
		<body>
    <!-- Wrapper -->
      <div id="wrapper">

        <!-- Header -->
          <header id="header" class="alt">
            <a href="/" class="logo"><strong>Destination Balkans</strong></a>
              <a href="http://www.georgemilojevic.net/" class="logo"><span>georgemilojevic.net</span></a>
            <nav>
              <a href="#menu">Menu</a>
            </nav>
          </header>

        <!-- Menu -->
          <nav id="menu">
            <ul class="links">
              <li><a href="/">Home</a></li>
              <li><a href="<?=$state[0]->getDbStateName()?>"><?=$state[0]->getDbStateName()?></a></li>
              <li><a href="<?=$state[1]->getDbStateName()?>"><?=$state[1]->getDbStateName()?></a></li>
              <li><a href="<?=$state[2]->getDbStateName()?>"><?=$state[2]->getDbStateName()?></a></li>
              <li><a href="<?=$state[3]->getDbStateName()?>"><?=$state[3]->getDbStateName()?></a></li>
              <li><a href="<?=$state[4]->getDbStateName()?>"><?=$state[4]->getDbStateName()?></a></li>
              <li><a href="<?=$state[5]->getDbStateName()?>"><?=$state[5]->getDbStateName()?></a></li>
              <li><a href="/generic">Generic</a></li>
            </ul>
            <ul class="actions vertical">
              <li><a href="#" class="button special fit">Get Started</a></li>
              <li><a href="#" class="button fit">Log In</a></li>
            </ul>
          </nav>

        <!-- Banner -->
          <section id="banner" class="major">
            <div class="inner">
              <header class="major">
                <h1>Welcome To Destination Balkans</h1>
              </header>
              <div class="content">
                <p>Places to go Off the Beaten Path</p>
                <ul class="actions">
                  <li><a href="#one" class="button next scrolly">Find Out More</a></li>
                </ul>
              </div>
            </div>
          </section>
        <!-- Main -->
          <div id="main">

            <!-- One -->
              <section id="one" class="tiles">
                <article>
                  <span class="image">
                    <img src="images/pic01.jpg" alt="" />
                  </span>
                  <header class="major">
                    <h3><a href="<?=$state[1]->getDbStateName()?>" class="link"><?=$state[1]->getDbStateName()?></a></h3>
                    <p>Bled Lake</p>
                  </header>
                </article>
                <article>
                  <span class="image">
                    <img src="images/pic02.jpg" alt="" />
                  </span>
                  <header class="major">
                    <h3><a href="<?=$state[2]->getDbStateName()?>" class="link"><?=$state[2]->getDbStateName()?></a></h3>
                    <p>Plitvice National Park</p>
                  </header>
                </article>
                <article>
                  <span class="image">
                    <img src="images/pic03.jpg" alt="" />
                  </span>
                  <header class="major">
                    <h3><a href="<?=$state[0]->getDbStateName()?>" class="link"><?=$state[0]->getDbStateName()?></a></h3>
                    <p>Uvac Canyon</p>
                  </header>
                </article>
                <article>
                  <span class="image">
                    <img src="images/pic04.jpg" alt="" />
                  </span>
                  <header class="major">
                    <h3><a href="<?=$state[3]->getDbStateName()?>" class="link"><?=$state[3]->getDbStateName()?></a></h3>
                    <p>Matka Canyon</p>
                  </header>
                </article>
                <article>
                  <span class="image">
                    <img src="images/pic05.jpg" alt="" />
                  </span>
                  <header class="major">
                    <h3><a href="<?=$state[5]->getDbStateName()?>" class="link"><?=$state[5]->getDbStateName()?></a></h3>
                    <p>Mostar Bridge</p>
                  </header>
                </article>
                <article>
                  <span class="image">
                    <img src="images/pic06.jpg" alt="" />
                  </span>
                  <header class="major">
                    <h3><a href="<?=$state[4]->getDbStateName()?>" class="link"><?=$state[4]->getDbStateName()?></a></h3>
                    <p>Boka Kotorska Bay</p>
                  </header>
                </article>
              </section>
            <!-- Two -->
              <section id="two">
                <div class="inner">
                  <header class="major">
                    <h2>Massa libero</h2>
                  </header>
                  <p>Nullam et orci eu lorem consequat tincidunt vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus pharetra. Pellentesque condimentum sem. In efficitur ligula tate urna. Maecenas laoreet massa vel lacinia pellentesque lorem ipsum dolor. Nullam et orci eu lorem consequat tincidunt. Vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus amet pharetra et feugiat tempus.</p>
                  <ul class="actions">
                    <li><a href="/destination" class="button next">Get Started</a></li>
                  </ul>
                </div>
              </section>
          </div>

        <!-- Contact -->
          <section id="contact">
            <div class="inner">
              <section>
                <form method="post" action="#">
                  <div class="field half first">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" />
                  </div>
                  <div class="field half">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" />
                  </div>
                  <div class="field">
                    <label for="message">Message</label>
                    <textarea name="message" id="message" rows="6"></textarea>
                  </div>
                  <ul class="actions">
                    <li><input type="submit" value="Send Message" class="special" /></li>
                    <li><input type="reset" value="Clear" /></li>
                  </ul>
                </form>
              </section>
              <section class="split">
                <section>
                  <div class="contact-method">
                    <span class="icon alt fa-envelope"></span>
                    <h3>Email</h3>
                    <a href="#">about@destinationbalkans.org</a>
                  </div>
                </section>
                <section>
                  <div class="contact-method">
                    <span class="icon alt fa-phone"></span>
                    <h3>Phone</h3>
                    <span>(381) 000-0000</span>
                  </div>
                </section>
                <section>
                  <div class="contact-method">
                    <span class="icon alt fa-home"></span>
                    <h3>Address</h3>
                    <span>Knez Mihailova no.7<br />
                    Belgrade, 11000<br />
                    Serbia</span>
                  </div>
                </section>
              </section>
            </div>
          </section>

        <!-- Footer -->
          <footer id="footer">
            <div class="inner">
              <ul class="icons">
                <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
              </ul>
              <ul class="copyright">
                <li>&copy; Untitled</li>
              </ul>
            </div>
          </footer>

      </div>
      <!-- Scripts -->
			<script src="<?php echo $view['assets']->getUrl('js/jquery.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrolly.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/jquery.scrollex.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/skel.min.js') ?>"></script>
			<script src="<?php echo $view['assets']->getUrl('js/util.js') ?>"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo $view['assets']->getUrl('js/main.js') ?>"></script>

		</body>
</html>
