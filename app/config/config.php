<?php

$container->loadFromExtension('assetic', array(
    'debug' => '%kernel.debug%',
    'use_controller' => '%kernel.debug%',
    'filters' => array(
        'cssrewrite' => null,
    ),
    'templating' => array(
        'engines' => array('twig', 'php'),
    ),
));
$configuration->loadFromExtension('doctrine', array(
    'dbal' => array(
        'driver'   => 'pdo_mysql',
        'host'     => '%database_host%',
        'dbname'   => '%database_name%',
        'user'     => '%database_user%',
        'password' => '%database_password%',
    ),
));
